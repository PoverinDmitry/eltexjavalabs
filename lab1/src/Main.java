import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;



public class Main {

    private final static int INSTR = 0;
    private final static int BUIMAT = 1;
    private final static int PNT = 2;
    private final static int EXT = -1;

    private static ItemFactory getItemFactory(int index)
    {
        ItemFactory obj = null;
        switch(index)
        {
            case INSTR:
                obj = new IntrumentsFactory();
                break;
            case BUIMAT:
                obj = new BuildingsMaterialsFactory();
                break;
            case PNT:
                obj = new PaintsFactory();
                break;

            default:
                System.out.println("Неверно указан тип товара");
                return null;
        }
        return obj;
    }

    public static void main(String[] args) throws IOException {
        int countObjects;

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Введите число объектов");
        countObjects = Integer.parseInt(in.readLine());

        System.out.println("Инструменты = 0");
        System.out.println("Строительные материалы = 1");
        System.out.println("Краски = 2");

        ICrudAction[] objects = new ICrudAction[countObjects];
        ICrudAction obj;

        ItemFactory itemFactory;
        int command;
        do{
            command = Integer.parseInt(in.readLine());
            itemFactory = getItemFactory(command);
        } while(itemFactory == null);

        for(int i = 0; i < countObjects; i++)
        {
            obj = itemFactory.createObject();
            obj.create();
            obj.read();
            objects[i] = obj;
        }

        boolean flag = true;
        do {

            System.out.println("Введите индекс объекта, который нужно изменить");
            command = Integer.parseInt(in.readLine());

            if(command == EXT)
                System.exit(0);
            else if(command >= countObjects || command < 0)
                System.out.println("Некорректный номмер объекта");
            else
                objects[command].update();

            for(int i = 0; i < countObjects; i++)
                objects[i].read();

        } while(flag);
        in.close();
    }
}
