public class PaintsFactory implements ItemFactory {
    @Override
    public ICrudAction createObject() {
        return new Paints();
    }
}
