import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class BuildingMaterials extends ACrudAction{

    protected float density; // плотность материала, кг / м^3
    protected float porosity; // пористость материала, %

    @Override
    public int delete()
    {
        super.delete();
        this.density = 0.0f;
        this.porosity = 0.0f;
        return 0;
    }

    @Override
    public int update() throws IOException {
        super.update();

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String buf;

        System.out.println("Введите плотность строительного материала (кг/m^3)");
        buf = in.readLine();
        this.density = Float.parseFloat(buf);

        System.out.println("Введите меру пористости строительного материала (%)");
        buf = in.readLine();
        this.porosity = Float.parseFloat(buf);
        in.close();

        return 0;
    }

    @Override
    public int create()
    {
        super.create();
        Random random = new Random();
        this.density = random.nextFloat();
        this.porosity = (float)(random.nextInt(10000)) / 100.0f;
        return 0;
    }

    @Override
    public int read()
    {
        super.read();
        String div = this.divOutput;
        outputValues("Плотность материала", this.density, div);
        outputValues("Пористость материала", this.porosity, div);
        return 0;
    }
}
