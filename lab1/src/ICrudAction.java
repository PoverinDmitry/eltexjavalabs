import java.io.IOException;
import java.util.UUID;

public interface ICrudAction {
    int create();
    int read();
    int update() throws IOException;
    int delete();
}