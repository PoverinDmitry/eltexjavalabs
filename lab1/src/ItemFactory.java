public interface ItemFactory {
    ICrudAction createObject();
}