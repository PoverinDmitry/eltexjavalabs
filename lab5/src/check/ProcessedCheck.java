package check;

import building_items.ACrudAction;
import shopping_items.Order;
import shopping_items.Orders;

import java.util.Iterator;

public class ProcessedCheck extends ACheck {
    public ProcessedCheck(Orders orders) {
        super(orders);
    }

    @Override
    public int check() {
        synchronized (orders) {
            for (Iterator<Order> it = this.orders.getIteratorList(); it.hasNext(); ) {
                Order item = it.next();
                if (item.isOrderProcessed()) {
                    it.remove();
                    this.outputInfo("Заказ: " + item.getId().toString() + " удалён из базы");
                }
            }
        }
        return 0;
    }

    @Override
    public void run()
    {
        while(true) {
            try {
                sleep(this.waitCheck);
                this.check();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
