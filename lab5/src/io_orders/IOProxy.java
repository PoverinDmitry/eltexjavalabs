package io_orders;

import building_items.ACrudAction;
import shopping_items.Orders;

import java.io.IOException;
import java.util.UUID;

public class IOProxy extends AManageOrder {
    private IOrder iorder;

    public IOProxy(Orders orders, FormatId id) {
        super(orders);
        this.setIorder(id);
    }

    public enum FormatId
    {
        JSON, BINARY
    }
    public IOrder setIorder(FormatId formatId)
    {
        switch(formatId)
        {
            case JSON:
                this.iorder = new ManagerOrderJSON(this.orders);
                break;
            case BINARY:
                this.iorder = new ManagerOrderFile(this.orders);
                break;
        }
        return null;
    }

    @Override
    public int readById(String fileName, UUID id) throws IOException, ClassNotFoundException {
        this.iorder.readById(fileName, id);
        return 0;
    }

    @Override
    public int saveById(String fileName, UUID id) throws IOException, ClassNotFoundException {
        this.iorder.saveById(fileName, id);
        return 0;
    }

    @Override
    public int readAll(String fileName) throws IOException, ClassNotFoundException {
        this.iorder.readAll(fileName);
        return 0;
    }

    @Override
    public int saveAll(String fileName) throws IOException {
        this.iorder.saveAll(fileName);
        return 0;
    }
}
