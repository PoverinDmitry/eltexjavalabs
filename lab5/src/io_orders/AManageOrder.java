package io_orders;

import shopping_items.Orders;

abstract public class AManageOrder implements IOrder {
    public AManageOrder(Orders orders)
    {
        this.orders = orders;
    }

    protected Orders orders;

}
