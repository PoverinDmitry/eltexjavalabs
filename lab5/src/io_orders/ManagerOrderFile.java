package io_orders;

import shopping_items.Order;
import shopping_items.Orders;

import java.io.*;
import java.util.Iterator;
import java.util.UUID;

public class ManagerOrderFile extends AManageOrder{
    public ManagerOrderFile(Orders orders) {
        super(orders);
    }

    @Override
    public int readById(String fileName, UUID id) throws IOException, ClassNotFoundException {

        Orders<Order> ordersTemp = new Orders<>();
        FileInputStream fileInputStream = new FileInputStream(fileName);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        ordersTemp = (Orders)objectInputStream.readObject();
        Order curOrder = ordersTemp.getOrderById(id);
        if(curOrder != null)
        {
            if(this.orders.setOrderById(id, curOrder) != 0)
                this.orders.add(curOrder);
        }
        else
            return 1;
        return 0;
    }

    @Override
    public int saveById(String fileName, UUID id) throws IOException, ClassNotFoundException {
        Order curOrder = this.orders.getOrderById(id);
        if(curOrder == null)
            return 1;

        FileInputStream fileInputStream = new FileInputStream(fileName);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

        Orders<Order> ordersTemp = new Orders<>();
        ordersTemp = (Orders)objectInputStream.readObject();
        fileInputStream.close();

        if(ordersTemp.setOrderById(id, curOrder) != 0)
            ordersTemp.add(curOrder);

        FileOutputStream fileOutputStream = new FileOutputStream(fileName);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(ordersTemp);
        objectOutputStream.close();

        return 0;
    }

    @Override
    public int readAll(String fileName) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(fileName);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

        Orders<Order> ordersTemp = new Orders<>();
        ordersTemp = (Orders)objectInputStream.readObject();
        this.orders.copyFrom(ordersTemp);
        objectInputStream.close();
        return 0;
    }

    @Override
    public int saveAll(String fileName) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(fileName);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

        objectOutputStream.writeObject(this.orders);
        objectOutputStream.close();
        return 0;
    }
}
