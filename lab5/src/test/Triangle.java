package test;

import com.fasterxml.jackson.annotation.JsonCreator;

public class Triangle extends Shape {

    @JsonCreator
    public Triangle()
    {
        this.name = "Triangle";
    }

    protected int point1 = 0;
    protected int point2 = 1;
    protected int point3 = 2;
}
