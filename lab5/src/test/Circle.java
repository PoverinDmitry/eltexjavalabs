package test;

import com.fasterxml.jackson.annotation.JsonCreator;

public class Circle extends Shape{

    @JsonCreator
    public Circle()
    {
        this.name = "Circle";
        this.centr = 0;
        this.radius = 5;
    }

    private double radius;
    private int centr;
}
