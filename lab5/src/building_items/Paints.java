package building_items;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;

public class Paints extends ACrudAction {

    protected StringBuilder typePaint; // Тип краски
    protected StringBuilder colorPaint;// Цвет краски

    public Paints()
    {
        super();
    }
    @Override
    public int delete()
    {
        super.delete();
        this.typePaint = null;
        this.colorPaint = null;
        return 0;
    }

    @Override
    public int update() throws IOException {
        super.update();

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String buf;

        System.out.println("Введите тип краски");
        buf = in.readLine();
        this.typePaint = new StringBuilder(buf);

        System.out.println("Введите цвет краски");
        buf = in.readLine();
        this.colorPaint = new StringBuilder(buf);

        this.typePaint = typePaint;
        this.colorPaint = colorPaint;
        in.close();
        return 0;
    }

    @Override
    public int create()
    {
        super.create();

        this.typePaint = this.getRandomString(this.minSizeRandomString, this.maxSizeRandomString);
        this.colorPaint = this.getRandomString(this.minSizeRandomString, this.maxSizeRandomString);

        return 0;
    }

    @Override
    public int read()
    {
        super.read();
        String div = this.divOutput;
        outputValues("Тип краски", this.typePaint, div);
        outputValues("Цвет краски", this.colorPaint, div);
        return 0;
    }
}