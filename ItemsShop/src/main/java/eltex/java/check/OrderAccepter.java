package eltex.java.check;

import eltex.java.building_items.ItemShop;
import eltex.java.building_items.utils.CollectionItems;
import eltex.java.shopping_items.Credentials;
import eltex.java.shopping_items.Order;
import eltex.java.shopping_items.Orders;
import eltex.java.shopping_items.ShoppingCart;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;

public class OrderAccepter extends Thread {
    public OrderAccepter(Orders orders, Credentials credentials, CollectionItems buildingItems) {
        this.orders = orders;
        this.credentials = credentials;
        this.collectionItems = buildingItems;
    }

    private Orders<Order> orders = new Orders<>();
    private Credentials credentials;
    private CollectionItems collectionItems;

    private long waitingTime = 2 * 1000;

    private int outputMessage(String msg)
    {
        System.out.println(msg);
        return 0;
    }

    public Order makeOrder()
    {
        Random random = new Random();
        int countBuildingItems = collectionItems.getCount();
        ShoppingCart shoppingCart = new ShoppingCart();

            for (Iterator<ItemShop> it = collectionItems.getIterator(); it.hasNext(); ) {
                ItemShop item = it.next();
                if (random.nextBoolean())
                    shoppingCart.add(item);
            }
        return new Order(credentials, shoppingCart);
    }

    public int makeOrders()
    {
        Random random = new Random();
        int countBuildingItems = collectionItems.getCount();
        ShoppingCart shoppingCart = new ShoppingCart();

        synchronized (orders) {
            for (Iterator<ItemShop> it = collectionItems.getIterator(); it.hasNext(); ) {
                ItemShop item = it.next();
                if (random.nextBoolean())
                    shoppingCart.add(item);
            }
            UUID orderId = orders.makePurchase(credentials, shoppingCart);
            outputMessage("Заказ " + orderId.toString() + " принят!");
        }
        return 0;
    }

    @Override
    public void run()
    {
        while(true) {
            try {
                sleep(this.waitingTime);
                this.makeOrders();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}