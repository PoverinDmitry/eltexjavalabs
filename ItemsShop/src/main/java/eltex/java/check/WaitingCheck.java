package eltex.java.check;

import eltex.java.shopping_items.Order;
import eltex.java.shopping_items.Orders;

import java.util.Iterator;

public class WaitingCheck extends ACheck {
    public WaitingCheck(Orders orders) {
        super(orders);
    }

    @Override
    public int check() {
        synchronized(orders) {
            for (Iterator<Order> it = this.orders.getIteratorList(); it.hasNext(); ) {
                Order item = it.next();
                if (!item.isOrderProcessed()) {
                    item.setStatus(Order.ISPROCESSED);
                    this.outputInfo("Заказ: " + item.getId().toString() + " обработан!");
                }
            }
        }
        return 0;
    }

    @Override
    public void run()
    {
        while(true) {
            try {
                sleep(this.waitCheck);
                this.check();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
