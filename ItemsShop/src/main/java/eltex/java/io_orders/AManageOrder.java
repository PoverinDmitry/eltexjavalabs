package eltex.java.io_orders;


import eltex.java.shopping_items.Orders;

abstract public class AManageOrder implements IOrder {
    public AManageOrder(Orders orders)
    {
        this.orders = orders;
    }

    protected Orders orders;

}
