package eltex.java.io_orders;

import eltex.java.building_items.ItemShop;
import eltex.java.shopping_items.Order;
import eltex.java.shopping_items.OrderNotFoundException;
import eltex.java.shopping_items.Orders;

import java.io.IOException;
import java.util.UUID;

public interface IOrder {
    Order readById(String fileName, UUID id) throws IOException, ClassNotFoundException, OrderNotFoundException;
    int saveById(String fileName, UUID id) throws IOException, ClassNotFoundException;
    Orders readAll(String fileName) throws IOException, ClassNotFoundException;
    int saveAll(String fileName) throws IOException;
    int addToCard(String fileName, ItemShop item, UUID id) throws IOException, OrderNotFoundException;
    int delById(String fileName, UUID id) throws IOException, OrderNotFoundException;
}