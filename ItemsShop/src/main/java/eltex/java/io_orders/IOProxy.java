package eltex.java.io_orders;


import eltex.java.building_items.ItemShop;
import eltex.java.shopping_items.Order;
import eltex.java.shopping_items.OrderNotFoundException;
import eltex.java.shopping_items.Orders;
import org.apache.log4j.Logger;


import java.io.IOException;
import java.util.UUID;

public class IOProxy extends AManageOrder {
    private IOrder iorder;
    private final static Logger logger = Logger.getLogger(IOProxy.class);

    public IOProxy(Orders orders, FormatId id) {
        super(orders);
        this.setIorder(id);
    }

    public enum FormatId
    {
        JSON, BINARY
    }

    public IOrder setIorder(FormatId formatId)
    {
        switch(formatId)
        {
            case JSON:
                this.iorder = new ManagerOrderJSON(this.orders);
                break;
            case BINARY:
                break;
        }
        return null;
    }

    @Override
    public Order readById(String fileName, UUID id) throws IOException, ClassNotFoundException, OrderNotFoundException {
        Order order = this.iorder.readById(fileName, id);
        if(order == null)
            throw new OrderNotFoundException();
        return order;
    }

    @Override
    public int saveById(String fileName, UUID id) throws IOException, ClassNotFoundException {
        return this.iorder.saveById(fileName, id);
    }

    @Override
    public Orders readAll(String fileName) throws IOException, ClassNotFoundException {
        return this.iorder.readAll(fileName);
    }

    @Override
    public int saveAll(String fileName) throws IOException {
        return this.iorder.saveAll(fileName);
    }

    @Override
    public int addToCard(String fileName, ItemShop item, UUID id) throws IOException, OrderNotFoundException {
        return this.iorder.addToCard(fileName, item, id);
    }

    @Override
    public int delById(String fileName, UUID id) throws IOException, OrderNotFoundException {
        return this.iorder.delById(fileName, id);
    }
}