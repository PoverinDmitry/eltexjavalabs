package eltex.java.shopping_items.repos;

import eltex.java.building_items.ItemShop;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<ItemShop, Long> {
}
