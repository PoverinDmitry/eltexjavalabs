package eltex.java.shopping_items.repos;

import eltex.java.shopping_items.Credentials;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CredentialsRepository extends JpaRepository<Credentials, Long> {
}
