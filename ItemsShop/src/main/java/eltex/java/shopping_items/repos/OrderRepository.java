package eltex.java.shopping_items.repos;

import eltex.java.shopping_items.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
}
