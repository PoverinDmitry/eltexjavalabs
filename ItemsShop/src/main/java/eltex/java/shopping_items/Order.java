package eltex.java.shopping_items;

import com.fasterxml.jackson.annotation.JsonIgnore;
import eltex.java.building_items.ItemShop;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "Orders")
public class Order implements Serializable {

    public Order() {
    }

    public Order(Credentials credentials, ShoppingCart shoppingCart)
    {
        this.isProcessed = ISWAITING; // Принимает значение истина, если заказ обработан
        this.creationTime = new Date();

        this.shoppingCart = shoppingCart;
        this.credentials = credentials;
        this.id = UUID.randomUUID();
        this.waitingTime = (Date) this.creationTime.clone();
        this.waitingTime.setSeconds(59);
    }

    public static final boolean ISPROCESSED = true; // Значение флага статуса заказа = обработано
    public static final boolean ISWAITING = false; // Значение флага статуса заказа = в обработке

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idOrder;

    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "credentials_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Credentials credentials;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "shopping_cart_id", nullable = false)
    @JsonIgnore
    private ShoppingCart shoppingCart;

    private boolean isProcessed;

    @Temporal(TemporalType.DATE)
    private Date creationTime;

    @Temporal(TemporalType.DATE)
    private Date waitingTime;

    @Transient
    @JsonIgnore
    public UUID getId()
    {
        return this.id;
    }

    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public int setStatus(boolean status)
    {
        this.isProcessed = status;
        return 0;
    }

    public int updateSatus()
    {
        if(waitingTime.before(new Date()))
            isProcessed = true;
        return 0;
    }

    public boolean isOrderProcessed() {
        return this.isProcessed;
    }

    public long getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(long idOrder) {
        this.idOrder = idOrder;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public boolean isProcessed() {
        return isProcessed;
    }

    public void setProcessed(boolean processed) {
        isProcessed = processed;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(Date waitingTime) {
        this.waitingTime = waitingTime;
    }

    public int copyFrom(Order order)
    {
        this.isProcessed = order.isProcessed;
        this.creationTime = order.creationTime;
        this.credentials = order.credentials;
        this.waitingTime = order.waitingTime;
        this.shoppingCart = order.shoppingCart;

        return 0;
    }

    public int read()
    {
        String div = ":\t";
        System.out.println("===========Заказ=========");
        ItemShop.outputValues("ID", this.id, div);
        ItemShop.outputValues("Дата создания", this.creationTime, div);
        ItemShop.outputValues("Дата исполнения", this.waitingTime, div);
        this.credentials.read();
        System.out.println("====Товары====");
        this.shoppingCart.read();
        return 0;
    }
}
