package eltex.java.shopping_items;

import eltex.java.building_items.ItemShop;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class ShoppingCart implements Serializable {

    public ShoppingCart() {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<ItemShop> getItems() {
        return items;
    }

    public void setItems(List<ItemShop> items) {
        this.items = items;
    }

    @ManyToMany(targetEntity = ItemShop.class,
            cascade = {CascadeType.ALL})
    private List<ItemShop> items = new ArrayList<>();

    public boolean isItemInShoppingcart(UUID id){
        return this.items.contains(id);
    }

    public int add(ItemShop item){
        this.items.add(item);
        return 0;
    }

    public int delete(ItemShop item){
        this.items.remove(item);
        return 0;
    }

    public boolean isEmpty(){
        return this.items.isEmpty();
    }

    public int read(){
        if(this.items.isEmpty()) {
            System.out.println("Пусто");
            return 0;
        }
        System.out.println("============Ваша корзина==========");
        for (ItemShop item: this.items) {
            item.read();
        }
        return 0;
    }
}
