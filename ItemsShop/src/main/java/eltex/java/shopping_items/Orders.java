package eltex.java.shopping_items;


import java.beans.Transient;
import java.io.Serializable;
import java.util.*;

//@JsonAutoDetect(getterVisibility = JsonAutoDetect.Visibility.ANY)
public class Orders<T extends Order> implements Serializable, Cloneable {

    public Orders()
    {
        this.ordersList = new LinkedList<>();
        this.orderHashMap = new LinkedHashMap<>();
    }

    private List<T> ordersList;

    private Map<UUID, T> orderHashMap;

    @Transient
    public Iterator<T> getIteratorList()
    {
        return this.ordersList.iterator();
    }

    public List<T> getOrdersList(){
        return this.ordersList;
    }

    public UUID makePurchase(Credentials credentials, ShoppingCart shoppingCart)
    {
        T order = (T)(new Order(credentials, shoppingCart));
        this.add(order);
        return order.getId();
    }

    public boolean add(T order)
    {
        boolean result = this.ordersList.add(order);
        if(result)
            this.orderHashMap.put(order.getId(), order);
        return result;
    }

    public boolean delete(T order)
    {
        boolean result = this.ordersList.remove(order);
        if(result)
            this.orderHashMap.remove(order);
        return result;
    }

    public Order getOrderById(UUID id)
    {
        return this.orderHashMap.get(id);
    }

    public int updateStatus()
    {
        for (T item: ordersList)
        {
            item.updateSatus();
            if(item.isOrderProcessed())
                this.delete(item);
        }
        return 0;
    }

    public boolean isEmpty()
    {
        return this.ordersList.isEmpty();
    }

    public int read()
    {
        System.out.println("====Заказы====");
        if(this.ordersList.isEmpty()) {
            System.out.println("Пусто");
            return 0;
        }
        for (T item: this.ordersList) {
            item.read();
        }
        return 0;
    }

    public int readId()
    {
        System.out.println("====Заказы====");
        if(this.ordersList.isEmpty()) {
            System.out.println("Пусто");
            return 1;
        }
        for (T item: this.ordersList) {
            System.out.println(item.getId());
        }
        return 0;
    }

    public int copyFrom(Orders orders)
    {
        this.ordersList = orders.ordersList;
        this.orderHashMap = orders.orderHashMap;
        return 0;
    }

}
