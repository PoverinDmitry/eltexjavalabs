package eltex.java.shopping_items;

import eltex.java.building_items.ItemShop;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "users")
public class Credentials implements Serializable {
    public Credentials() {

    }
    public Credentials(String ln,
                       String fn,
                       String pat,
                       String email)
    {
        this.id = UUID.randomUUID();
        this.lastName = ln;
        this.firstName = fn;
        this.patronymic = pat;
        this.email = email;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userId;

    protected UUID id;
    protected String lastName;
    protected String firstName;
    protected String patronymic;
    protected String email;

    public int read()
    {
        String div = ":\t";
        System.out.println("====Клиент====");
        ItemShop.outputValues("ID", this.id, div);
        ItemShop.outputValues("ИМЯ", this.firstName, div);
        ItemShop.outputValues("ФАМИЛИЯ", this.lastName, div);
        ItemShop.outputValues("ОТЧЕСТВО", this.patronymic, div);
        ItemShop.outputValues("EMAIL", this.email, div);
        return 0;
    }
}