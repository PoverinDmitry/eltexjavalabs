package eltex.java.building_items;

import eltex.java.utils.Helper;

import javax.persistence.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Entity
public class Paints extends ItemShop {

    protected String typePaint; // Тип краски
    protected String colorPaint;// Цвет краски

    public Paints()
    {
        super();
    }
    @Override
    public int delete()
    {
        super.delete();
        this.typePaint = null;
        this.colorPaint = null;
        return 0;
    }

    @Override
    public int update() throws IOException {
        super.update();

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String buf;

        System.out.println("Введите тип краски");
        buf = in.readLine();
        this.typePaint = buf;

        System.out.println("Введите цвет краски");
        buf = in.readLine();
        this.colorPaint = buf;

        this.typePaint = typePaint;
        this.colorPaint = colorPaint;
        in.close();
        return 0;
    }

    @Override
    public int create()
    {
        super.create();

        this.typePaint = Helper.getRandomString();
        this.colorPaint = Helper.getRandomString();

        return 0;
    }

    @Override
    public int read()
    {
        super.read();
        String div = this.divOutput;
        outputValues("Тип краски", this.typePaint, div);
        outputValues("Цвет краски", this.colorPaint, div);
        return 0;
    }
}