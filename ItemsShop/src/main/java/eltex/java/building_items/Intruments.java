package eltex.java.building_items;

import eltex.java.utils.Helper;

import javax.persistence.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Entity
public class Intruments extends ItemShop {

    public Intruments()
    {
        super();
    }
    protected String typeInstrument;

    @Override
    public int delete()
    {
        super.delete();
        this.typeInstrument = null;
        return 0;
    }

    @Override
    public int update() throws IOException {
        super.update();

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String buf;

        System.out.println("Введите тип интрумента (Ручной, электронный, измерительный и т.д.)");
        buf = in.readLine();
        this.typeInstrument = buf;
        in.close();

        return 0;
    }

    @Override
    public int create()
    {
        super.create();
        this.typeInstrument = Helper.getRandomString();
        return 0;
    }

    @Override
    public int read()
    {
        super.read();
        String div = this.divOutput;
        outputValues("Тип интрумента", this.typeInstrument, div);
        return 0;
    }
}
