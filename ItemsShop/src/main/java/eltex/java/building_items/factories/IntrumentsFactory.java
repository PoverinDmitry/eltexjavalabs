package eltex.java.building_items.factories;

import eltex.java.building_items.ICrudAction;
import eltex.java.building_items.Intruments;

public class IntrumentsFactory implements ItemFactory {
    @Override
    public ICrudAction createObject() {
        return new Intruments();
    }
}