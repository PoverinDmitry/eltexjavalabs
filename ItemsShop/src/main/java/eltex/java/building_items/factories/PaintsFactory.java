package eltex.java.building_items.factories;

import eltex.java.building_items.ICrudAction;
import eltex.java.building_items.Paints;

public class PaintsFactory implements ItemFactory {
    @Override
    public ICrudAction createObject() {
        return new Paints();
    }
}
