package eltex.java.building_items.factories;

import eltex.java.building_items.BuildingMaterials;
import eltex.java.building_items.ICrudAction;

public class BuildingsMaterialsFactory implements ItemFactory {
    @Override
    public ICrudAction createObject() {
        return new BuildingMaterials();
    }
}
