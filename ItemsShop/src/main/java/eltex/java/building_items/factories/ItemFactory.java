package eltex.java.building_items.factories;

import eltex.java.building_items.ICrudAction;

public interface ItemFactory {
    ICrudAction createObject();
}