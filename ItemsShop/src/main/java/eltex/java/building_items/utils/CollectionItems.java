package eltex.java.building_items.utils;

import eltex.java.building_items.ItemShop;

import java.io.Serializable;
import java.util.*;

public class CollectionItems implements Serializable {

    public CollectionItems()
    {
        this.CrudActionList = new LinkedList<>();
        this.CrudActionIdSet = new TreeSet<>();
    }

    public int add(ItemShop item)
    {
        if(!this.CrudActionIdSet.contains(item.getID()))
        {
            this.CrudActionList.add(item);
            this.CrudActionIdSet.add(item.getID());
        }
        return 0;
    }

    public boolean itemIsAvailable(ItemShop item)
    {
        return this.CrudActionIdSet.contains(item);
    }

    public int read()
    {
        for (ItemShop item:this.CrudActionList
             ) {
            item.read();
        }
        return 0;
    }

    public int getCount()
    {
        return this.CrudActionList.size();
    }
    public ItemShop getItem(int index)
    {
        return this.CrudActionList.get(index);
    }
    public Iterator<ItemShop> getIterator()
    {
        return this.CrudActionList.iterator();
    }

    private List<ItemShop> CrudActionList;
    private Set<UUID> CrudActionIdSet;
}