package eltex.java.building_items;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import eltex.java.shopping_items.ShoppingCart;
import eltex.java.utils.Helper;

import javax.persistence.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS,
        include = JsonTypeInfo.As.PROPERTY,
        property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BuildingMaterials.class),
        @JsonSubTypes.Type(value = Paints.class),
        @JsonSubTypes.Type(value = Intruments.class)
})
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
abstract public class ItemShop implements ICrudAction {
    public ItemShop()
    {
        this.ID = UUID.randomUUID();
    }

    public long getIdent() {
        return ident;
    }

    public void setIdent(long ident) {
        this.ident = ident;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected long ident;

    protected UUID ID;           // Идентификатор
    protected String name;       // Наименование товара
    protected String vendorCode; // Артикул
    protected float cost;        // Цена
    protected String companyMan; // Производитель

    protected static String divOutput = ":\t";
    private static int countObjects = 0;

    public void setID(UUID ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public String getCompanyMan() {
        return companyMan;
    }

    public void setCompanyMan(String companyMan) {
        this.companyMan = companyMan;
    }

    public static String getDivOutput() {
        return divOutput;
    }

    public static void setDivOutput(String divOutput) {
        ItemShop.divOutput = divOutput;
    }

    public static int getCountObjects() {
        return countObjects;
    }

    public UUID getID() {
        return ID;
    }

    @Override
    public int create()
    {
        countObjects++;
        this.name = Helper.getRandomString();
        this.companyMan = Helper.getRandomString();
        this.vendorCode = Helper.getRandomString();
        this.cost = new Random().nextFloat();
        return 0;
    }

    @Override
    public int delete()
    {
        //this.ID = null;
        this.name = null;
        this.vendorCode = null;
        this.cost = 0.0f;
        this.companyMan = null;

        this.countObjects--;
        return 0;
    }

    @Override
    public int update() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String buf;

        System.out.println("Введите наименование товара");
        buf = in.readLine();
        this.name = buf;

        System.out.println("Введите артикул товара");
        buf = in.readLine();
        this.vendorCode = buf;

        System.out.println("Введите цену товара");
        buf = in.readLine();
        this.cost = Float.parseFloat(buf);

        System.out.println("Введите производителя товара");
        buf = in.readLine();
        this.companyMan = buf;
        in.close();
        return 0;
    }

    public static String outputValues(Object key, Object value, String div)
    {
        String result = key.toString() + div + value.toString();
        System.out.println(result);
        return result;
    }

    @Override
    public int read()
    {
        String div = this.divOutput;
        System.out.println("===========================================");
        outputValues("ID", this.ID, div);
        outputValues("Наименование", this.name, div);
        outputValues("Артикул", this.vendorCode, div);
        outputValues("Цена", this.cost, div);
        outputValues("Производитель", this.companyMan, div);

        return 0;
    }
}
