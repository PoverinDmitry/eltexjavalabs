package eltex.java.utils;

public class LexemsCommands {
    public static final String READ_BY_ID = "readById";     // Лексема команды чтения заказа по идентификатору
    public static final String READ_ALL = "readAll";        // Лексема команды чтения всех заказов
    public static final String ADD_TO_CARD = "addToCard";   // Лексема команды добавления товара в корзину
    public static final String DEL_BY_ID = "delById";       // Лексема команды удаления заказа или товара по идетификатору

    public static final String COMMAND = "command";         // Лексема команды
    public static final String ORDER_ID = "order_id";       // Лексема заказа
    public static final String CARD_ID = "card_id";          // Лексема товара
    public static final String DEFAULT = "/";               //
}
