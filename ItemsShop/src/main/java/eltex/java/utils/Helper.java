package eltex.java.utils;

import eltex.java.building_items.ItemShop;
import eltex.java.building_items.utils.CollectionItems;
import eltex.java.building_items.factories.BuildingsMaterialsFactory;
import eltex.java.building_items.factories.ItemFactory;
import eltex.java.shopping_items.Credentials;
import eltex.java.shopping_items.Order;
import eltex.java.shopping_items.Orders;
import eltex.java.shopping_items.ShoppingCart;
import eltex.java.shopping_items.repos.CredentialsRepository;
import eltex.java.shopping_items.repos.ItemRepository;
import eltex.java.shopping_items.repos.OrderRepository;
import eltex.java.shopping_items.repos.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public class Helper {

    private static int countItems = 1;
    private static int countOrders = 10;

    protected static int minSizeRandomString = 5;
    protected static int maxSizeRandomString = 10;

    @Autowired
    private static OrderRepository orderRepository;

    @Autowired
    private static ShoppingCartRepository shoppingCartRepository;

    @Autowired
    private static ItemRepository itemRepository;

    @Autowired
    private static CredentialsRepository credentialsRepository;

    public static CollectionItems createItems(){
        CollectionItems buildingItems = new CollectionItems();
        ItemFactory itemFactory = new BuildingsMaterialsFactory();

        for(int i = 0; i < countItems; i++)
        {
            ItemShop item = (ItemShop)itemFactory.createObject();
            item.create();
            buildingItems.add(item);
        }
        return buildingItems;
    }

    public static List<ItemShop> createItemsList(){
        List<ItemShop> buildingItems = new ArrayList<>();
        ItemFactory itemFactory = new BuildingsMaterialsFactory();

        for(int i = 0; i < countItems; i++)
        {
            ItemShop item = (ItemShop)itemFactory.createObject();
            item.create();
            buildingItems.add(item);
        }
        return buildingItems;
    }

    public static Credentials generateCred()
    {
        Credentials credentials = new Credentials(
                new String("Иванов"),
                new String("Иван"),
                new String("Иванович"),
                new String("ivanov@ivan.ru")
        );
        return credentials;
    }

    public static Orders makeOrders()
    {
        Orders<Order> orders = new Orders<>();
        CollectionItems buildingItems = createItems();
        Credentials credentials = generateCred();

        Random random = new Random();
        int countBuildingItems = buildingItems.getCount();
        ShoppingCart shoppingCart = new ShoppingCart();

        synchronized (orders) {
            for(int i = 0; i < countOrders; i++) {
                for (Iterator<ItemShop> it = buildingItems.getIterator(); it.hasNext(); ) {
                    ItemShop item = it.next();
                    if (random.nextBoolean())
                        shoppingCart.add(item);
                }
                UUID orderId = orders.makePurchase(credentials, shoppingCart);
            }
        }
        return orders;
    }

    public static Orders makeOrdersJPA(){
        Orders<Order> orders = new Orders<>();
        List<ItemShop> buildingItems = createItemsList();

        ItemShop item;

        ItemFactory itemFactory = new BuildingsMaterialsFactory();
        item = (ItemShop) itemFactory.createObject();
        item.create();
        itemRepository.save(item);

        item = (ItemShop) itemFactory.createObject();
        item.create();
        itemRepository.save(item);

        item = (ItemShop) itemFactory.createObject();
        item.create();
        itemRepository.save(item);

        item = (ItemShop) itemFactory.createObject();
        item.create();
        itemRepository.save(item);

        Credentials credentials = generateCred();
        credentialsRepository.save(credentials);

        Random random = new Random();
        int countBuildingItems = buildingItems.size();

        synchronized (orders) {
            for(int i = 0; i < countOrders; i++) {
                ShoppingCart shoppingCart = new ShoppingCart();
                for (ItemShop elem: buildingItems) {
                    if (random.nextBoolean())
                        shoppingCart.add(elem);
                }
                shoppingCartRepository.save(shoppingCart);
                Order order = new Order(credentials, shoppingCart);
                orderRepository.save(order);
                UUID orderId = orders.makePurchase(credentials, shoppingCart);
            }
        }
        return orders;
    }

    public static String getRandomString(int minSize, int maxSize)
    {
        Random random = new Random();

        int length;
        if(minSize != maxSize)
            length = random.nextInt(maxSize - minSize) + minSize;
        else
            length = minSize;

        StringBuilder result = new StringBuilder();
        char minChar = 'a';
        char maxChar = 'z';
        for(int i = 0; i < length; i++) {
            char ch = (char) ((char) random.nextInt(maxChar - minChar) + minChar);
            result.append(ch);
        }
        return result.toString();
    }

    public static String getRandomString()
    {
        int minSize = minSizeRandomString;
        int maxSize = maxSizeRandomString;
        Random random = new Random();

        int length;
        if(minSize != maxSize)
            length = random.nextInt(maxSize - minSize) + minSize;
        else
            length = minSize;

        StringBuilder result = new StringBuilder();
        char minChar = 'a';
        char maxChar = 'z';
        for(int i = 0; i < length; i++) {
            char ch = (char) ((char) random.nextInt(maxChar - minChar) + minChar);
            result.append(ch);
        }
        return result.toString();
    }

}