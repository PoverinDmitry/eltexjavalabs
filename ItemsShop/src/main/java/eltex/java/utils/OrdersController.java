package eltex.java.utils;

import eltex.java.building_items.ItemShop;
import eltex.java.building_items.factories.BuildingsMaterialsFactory;
import eltex.java.building_items.factories.ItemFactory;
import eltex.java.shopping_items.*;
import eltex.java.shopping_items.repos.CredentialsRepository;
import eltex.java.shopping_items.repos.ItemRepository;
import eltex.java.shopping_items.repos.OrderRepository;
import eltex.java.shopping_items.repos.ShoppingCartRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
public class OrdersController {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ShoppingCartRepository shoppingCartRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    CredentialsRepository credentialsRepository;

    public OrdersController() throws IOException {
        Orders<Order> orders = Helper.makeOrders();
    }

    private final static Logger logger = Logger.getLogger(OrdersController.class);

    @RequestMapping(params = { LexemsCommands.COMMAND, LexemsCommands.CARD_ID }, method = RequestMethod.GET)
    public Object addToCard(@RequestParam(value = LexemsCommands.COMMAND) String command,
                            @RequestParam(value = LexemsCommands.CARD_ID) Long id) {
            switch (command) {
                case LexemsCommands.ADD_TO_CARD:
                    logger.info("Вызов команды добаления товара в корзину id=" + id.toString());
                    ItemFactory itemFactory = new BuildingsMaterialsFactory();
                    ItemShop item = (ItemShop) itemFactory.createObject();
                    item.create();

                    Optional<ShoppingCart> optionalShoppingCart = shoppingCartRepository.findById(id);
                    ShoppingCart shoppingCart = optionalShoppingCart.get();
                    if(shoppingCart != null) {
                        itemRepository.save(item);
                        shoppingCart.add(item);
                        shoppingCartRepository.save(shoppingCart);
                        logger.info("Команда успешно выполнена");
                    }
                    else
                        logger.error("Корзина не найдена");

                    return shoppingCartRepository.findAll();
                default:
                    return null;
            }
    }

    @RequestMapping(params = { LexemsCommands.COMMAND, LexemsCommands.ORDER_ID }, method = RequestMethod.GET)
    public Object readOrDelById(@RequestParam(value = LexemsCommands.COMMAND) String command,
                                @RequestParam(value = LexemsCommands.ORDER_ID) Long id) {
            switch (command) {
                case LexemsCommands.READ_BY_ID: {
                    logger.info("Вызов команды чтения заказа id=" + id.toString());
                    Optional<Order> orderOptinal = orderRepository.findById(id);
                    Order order = orderOptinal.get();
                    if(order != null)
                        logger.info("Команда успешно выполнена");
                    else
                        logger.error("Заказ не найден!");
                    return order;
                }
                case LexemsCommands.DEL_BY_ID: {
                    logger.info("Вызов команды удаления заказа id=" + id.toString());
                    orderRepository.deleteById(id);
                    List<Order> orders = orderRepository.findAll();
                    logger.info("Команда успешно выполнена");
                    return orders;
                }
                default:
                    logger.error("Неизвестная команда! Код ошибки: " + ErrorCodes.COMMAND_NOT_FOUND);
                    return null;
            }

    }

    @RequestMapping(params = LexemsCommands.COMMAND, method = RequestMethod.GET)
    public Object readAll(@RequestParam(value = LexemsCommands.COMMAND) String command) {

            switch (command) {
                case LexemsCommands.READ_ALL: {
                    logger.info("Вызов команды чтения всех заказов");
                    List<Order> orders = orderRepository.findAll();
                    logger.info("Команда чтения всех объектов успешно выполнена");
                    return orders;
                }
                default: {
                    logger.error("Неизвестная команда! Код ошибки: " + ErrorCodes.COMMAND_NOT_FOUND);
                    return null;
                }
            }
    }
}