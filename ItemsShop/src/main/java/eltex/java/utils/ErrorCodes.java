package eltex.java.utils;

public class ErrorCodes {
    public static final Integer SUCCESS = 0;
    public static final Integer ORDER_NOT_FOUND = 1;    // Заказ не найден
    public static final Integer FILE_IS_BROKEN = 2;     // Файл повреждён
    public static final Integer COMMAND_NOT_FOUND = 3;  // Команда не найдена
    public static final Integer CLASS_NOT_FOUND = 4;    // Ошибка чтения json
}
