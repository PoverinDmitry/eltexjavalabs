package eltex.java;

import eltex.java.building_items.ItemShop;
import eltex.java.building_items.factories.BuildingsMaterialsFactory;
import eltex.java.building_items.factories.IntrumentsFactory;
import eltex.java.building_items.factories.ItemFactory;
import eltex.java.building_items.factories.PaintsFactory;
import eltex.java.shopping_items.Credentials;
import eltex.java.shopping_items.Order;
import eltex.java.shopping_items.Orders;
import eltex.java.shopping_items.ShoppingCart;
import eltex.java.shopping_items.repos.CredentialsRepository;
import eltex.java.shopping_items.repos.ItemRepository;
import eltex.java.shopping_items.repos.OrderRepository;
import eltex.java.shopping_items.repos.ShoppingCartRepository;
import eltex.java.utils.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@SpringBootApplication
@EnableJpaAuditing
public class Lab8Application implements CommandLineRunner {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ShoppingCartRepository shoppingCartRepository;

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    CredentialsRepository credentialsRepository;

    public static void main(String[] args) {
        SpringApplication.run(Lab8Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        //credentialsRepository.deleteAll();
        //orderRepository.deleteAll();
        //shoppingCartRepository.deleteAll();
        //itemRepository.deleteAll();

        List<ItemShop> buildingItems = new ArrayList<>();

        int countBM = 3, countP = 3, countI = 3;
        int coubtShC = 5;
        ItemShop item;

        ItemFactory itemFactory = new BuildingsMaterialsFactory();
        for(int i = 0; i < countBM;i++){
            item = (ItemShop) itemFactory.createObject();
            item.create();
            buildingItems.add(item);
            itemRepository.save(item);
        }

        itemFactory = new IntrumentsFactory();
        for(int i = 0; i < countI;i++){
            item = (ItemShop) itemFactory.createObject();
            item.create();
            itemRepository.save(item);
        }

        itemFactory = new PaintsFactory();
        for(int i = 0; i < countP;i++){
            item = (ItemShop) itemFactory.createObject();
            item.create();
            itemRepository.save(item);
        }

        Credentials credentials = Helper.generateCred();
        credentialsRepository.save(credentials);

        Random random = new Random();
        for (int i = 0; i < coubtShC; i++) {
            ShoppingCart shoppingCart = new ShoppingCart();
            for (ItemShop elem : buildingItems) {
                if (random.nextBoolean())
                    shoppingCart.add(elem);
            }
            shoppingCartRepository.save(shoppingCart);
            Order order = new Order(credentials, shoppingCart);
            orderRepository.save(order);
        }
    }
}
