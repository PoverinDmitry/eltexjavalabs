package shopping_items;

import building_items.ACrudAction;

import java.util.*;

public class Orders<T extends Order> {

    private List<T> ordersList = new LinkedList<>();
    private Map<Date, T> orderHashMap = new LinkedHashMap<>();

    public Iterator<T> getIteratorList()
    {
        return this.ordersList.iterator();
    }

    public UUID makePurchase(Credentials credentials, ShoppingCart<ACrudAction> shoppingCart)
    {
        T order = (T)(new Order(credentials, shoppingCart));
        this.add(order);
        return order.getId();
    }

    public int add(T order)
    {
        this.ordersList.add(order);
        this.orderHashMap.put(new Date(), order);
        return 0;
    }
    public int delete(T order)
    {
        this.ordersList.remove(order);
        this.orderHashMap.remove(order);
        return 0;
    }

    public int updateStatus()
    {
        for (T item: ordersList)
        {
            item.updateSatus();
            if(item.isOrderProcessed())
                this.delete(item);
        }

        return 0;
    }

    public boolean isEmpty()
    {
        return this.ordersList.isEmpty();
    }

    public int read()
    {
        System.out.println("====Заказы====");
        if(this.ordersList.isEmpty()) {
            System.out.println("Пусто");
            return 0;
        }
        for (T item: this.ordersList) {
            item.read();
        }
        return 0;
    }

    public int readId()
    {
        System.out.println("====Заказы====");
        if(this.ordersList.isEmpty()) {
            System.out.println("Пусто");
            return 0;
        }
        for (T item: this.ordersList) {
            System.out.println(item.getId());
        }
        return 0;
    }
}
