package building_items;

public class PaintsFactory implements ItemFactory {
    @Override
    public ICrudAction createObject() {
        return new Paints();
    }
}
