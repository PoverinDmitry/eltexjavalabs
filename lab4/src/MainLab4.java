import building_items.*;
import check.ACheck;
import check.OrderAccepter;
import check.ProcessedCheck;
import check.WaitingCheck;
import shopping_items.*;

import javax.print.attribute.standard.OrientationRequested;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainLab4 {

    private static final int SEL = 1; // SEL ~ SELECT Режим добаления товара в корзину
    private static final int PUR = 2; // PUR ~ PURCHASE Режим оформления покупки
    private static final int SHW = 3; // SHW ~ SHOW Показать все покупки
    private static final int SHW_ACT_OR = 4; // SHW_ACT_OR ~ SHOW ACTIVE ORDERS Показать все активные заказы
    private static final int EXT = 0; // EXT ~ EXIT Завершение работы программы

    private static int selectItem(BuildingItems items, ShoppingCart<ACrudAction> shoppingCart, BufferedReader in) throws IOException {
        int command;
        int countItems = items.getCount();

        boolean flag = true;
        do {

            System.out.println("Список товаров");
            items.read();
            System.out.println("=================================");
            System.out.println("Введите номер товара, который вы хотите добавить в корзину:\tот 1 до " + String.valueOf(countItems));
            System.out.println("Выход:\t0");
            command = Integer.parseInt(in.readLine());
            if(command == EXT)
                return 0;
            else if(command < 1 || command > countItems)
                System.out.println("Неверно введена команда");
            else
                shoppingCart.add(items.getItem(command - 1));
        } while(flag);
        return 0;
    }

    private static BuildingItems createItems()
    {
        BuildingItems buildingItems = new BuildingItems();
        int countItems = 5;
        ItemFactory itemFactory = new BuildingsMaterialsFactory();

        for(int i = 0; i < countItems; i++)
        {
            ACrudAction item = (ACrudAction)itemFactory.createObject();
            item.create();
            buildingItems.add(item);
        }
        return buildingItems;
    }

    private static Credentials inputCredentials(BufferedReader in) throws IOException {
        System.out.println("Введите ИМЯ, ФАМИЛИЮ, ОТЧЕТВО, ваш email");

        String fname, lname, pat, email;

        fname = in.readLine();
        lname = in.readLine();
        pat = in.readLine();
        email= in.readLine();
        Credentials credentials = new Credentials(
                new StringBuilder(lname),
                new StringBuilder(fname),
                new StringBuilder(pat),
                new StringBuilder(email)
        );
        return credentials;
    }
    public static void helperFunctionLabWork3() throws IOException {
        BuildingItems buildingItems = createItems();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        Credentials credentials = inputCredentials(in);
        int command;
        boolean flag = true;


        ShoppingCart<ACrudAction> shoppingCart = new ShoppingCart<>();
        Orders<Order> orders = new Orders<>();

        ACheck processedChecker = new ProcessedCheck(orders);
        processedChecker.start();
        ACheck waitingChecker = new WaitingCheck(orders);
        waitingChecker.start();

        do {
            System.out.println("=================================");
            System.out.println("Выбрать товар:\t1");
            System.out.println("Оформить покупку:\t2");
            System.out.println("Показать содержимое корзины:\t3");
            System.out.println("Показать все активные заказы:\t4");
            System.out.println("Выход:\t0");
            command = Integer.parseInt(in.readLine());

            switch(command)
            {
                case SEL:
                    selectItem(buildingItems, shoppingCart, in);
                    break;
                case PUR:
                    if(!shoppingCart.isEmpty()) {
                        orders.makePurchase(credentials, shoppingCart);
                        shoppingCart = new ShoppingCart<>();
                    }
                    else System.out.println("Корзина пуста");
                    break;
                case SHW:
                    shoppingCart.read();
                    break;
                case SHW_ACT_OR:
                    orders.readId();
                    break;
                case EXT:
                    flag = false;
                    break;
                default:
                    System.out.println("Неверная команда!");
            }
        } while(flag);
        processedChecker.stop();
        waitingChecker.stop();
        in.close();
    }

    public static void helperFunctionLabWork4() throws IOException {
        BuildingItems buildingItems = createItems();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        Credentials credentials = inputCredentials(in);
        int command;
        boolean flag = true;

        ShoppingCart<ACrudAction> shoppingCart = new ShoppingCart<>();
        Orders<Order> orders = new Orders<>();

        ACheck processedChecker = new ProcessedCheck(orders);
        processedChecker.start();
        ACheck waitingChecker = new WaitingCheck(orders);
        waitingChecker.start();
        OrderAccepter orderAccepter = new OrderAccepter(orders, credentials, buildingItems);
        orderAccepter.start();

        do {
            System.out.println("=================================");
            System.out.println("Выход:\t0");

            command = Integer.parseInt(in.readLine());

            switch(command)
            {
                case EXT:
                    flag = false;
                    break;
                default:
                    System.out.println("Неверная команда!");
            }
        } while(flag);
        orderAccepter.stop();
        processedChecker.stop();
        waitingChecker.stop();
        in.close();
    }

    public static void main(String[] arg) throws IOException {

        helperFunctionLabWork4();
    }
}
