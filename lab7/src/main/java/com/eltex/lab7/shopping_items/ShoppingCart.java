package com.eltex.lab7.shopping_items;

import com.eltex.lab7.building_items.ACrudAction;

import java.io.Serializable;
import java.util.*;

public class ShoppingCart<T extends ACrudAction> implements Serializable {

    public ShoppingCart()
    {

    }

    private List<T> shoppingCarts = new ArrayList<>();
    //private Set<UUID> setItemsId = new TreeSet<>();

    public boolean isItemInShoppingcart(UUID id)
    {
        //return this.setItemsId.contains(id);
        return this.shoppingCarts.contains(id);
    }

    public int add(T item)
    {
        this.shoppingCarts.add(item);
        //this.setItemsId.add(item.getID());
        return 0;
    }

    public int delete(T item)
    {
        this.shoppingCarts.remove(item);
        //this.setItemsId.remove(item.getID());
        return 0;
    }

    public boolean isEmpty()
    {
        return this.shoppingCarts.isEmpty();
    }

    public int read()
    {
        if(this.shoppingCarts.isEmpty()) {
            System.out.println("Пусто");
            return 0;
        }
        System.out.println("============Ваша корзина==========");
        for (T item: this.shoppingCarts) {
            item.read();
        }
        return 0;
    }
}
