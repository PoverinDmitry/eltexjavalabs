package com.eltex.lab7;

import com.eltex.lab7.building_items.ACrudAction;
import com.eltex.lab7.building_items.BuildingsMaterialsFactory;
import com.eltex.lab7.building_items.ItemFactory;
import com.eltex.lab7.io_orders.IOProxy;
import com.eltex.lab7.shopping_items.Order;
import com.eltex.lab7.shopping_items.OrderNotFoundException;
import com.eltex.lab7.shopping_items.Orders;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@RestController
public class OrdersController {

    public OrdersController() throws IOException {
        Orders<Order> orders = Helper.makeOrders();
        File file = new File(this.nameFile);
        if(file.exists())
            file.delete();
        IOProxy io = new IOProxy(orders, IOProxy.FormatId.JSON);
        io.saveAll(nameFile);
    }

    private Integer countRequest = 0;
    private IOProxy ioProxy = new IOProxy(null, IOProxy.FormatId.JSON);
    private String nameFile = "OrdersForLab7.json";
    private final static Logger logger = Logger.getLogger(OrdersController.class);

    @RequestMapping(params = { LexemsCommands.COMMAND, LexemsCommands.CARD_ID }, method = RequestMethod.GET)
    public Orders addToCard(@RequestParam(value = LexemsCommands.COMMAND) String command,
                          @RequestParam(value = LexemsCommands.CARD_ID) String id) {
        try {
            switch (command) {
                case LexemsCommands.ADD_TO_CARD:
                    ItemFactory itemFactory = new BuildingsMaterialsFactory();
                    ACrudAction item = (ACrudAction) itemFactory.createObject();
                    item.create();
                    UUID uuid = UUID.fromString(id);
                    ioProxy.addToCard(this.nameFile, item, uuid);
                    Orders orders = ioProxy.readAll(this.nameFile);
                    return orders;
                default:
                    return null;
            }
        }
        catch (IOException e){
            logger.error("Ошибка чтения файла " + this.nameFile);
            logger.error(e.getMessage());
        }
        catch (ClassNotFoundException e){
            logger.error("Ошибка синтаксического анализа файла " + this.nameFile + ". Класс не найден!");
            logger.error(e.getMessage());
        }
        catch (OrderNotFoundException e){
            logger.error("Ошибка! Заказ " + id + " не найден! Код ошибки " + ErrorCodes.ORDER_NOT_FOUND);
        }
        finally {
            return null;
        }
    }

    @RequestMapping(params = { LexemsCommands.COMMAND, LexemsCommands.ORDER_ID }, method = RequestMethod.GET)
    public Object readOrDelById(@RequestParam(value = LexemsCommands.COMMAND) String command,
                          @RequestParam(value = LexemsCommands.ORDER_ID) String id) {

        try {
            switch (command) {
                case LexemsCommands.READ_BY_ID: {
                    UUID uuid = UUID.fromString(id);
                    Order order = ioProxy.readById(this.nameFile, uuid);
                    return order;
                }
                case LexemsCommands.DEL_BY_ID: {
                    UUID uuid = UUID.fromString(id);
                    ioProxy.delById(this.nameFile, uuid);
                    Orders orders = ioProxy.readAll(this.nameFile);
                    return orders;
                }
                default:
                    return null;
            }
        }
        catch (IOException e){
            logger.error("Ошибка чтения файла " + this.nameFile);
            logger.error(e.getMessage());
        }
        catch (ClassNotFoundException e){
            logger.error("Ошибка синтаксического анализа файла " + this.nameFile + ". Класс не найден!");
            logger.error(e.getMessage());
        }
        catch (OrderNotFoundException e){
            logger.error("Ошибка! Заказ " + id + " не найден! Код ошибки " + ErrorCodes.ORDER_NOT_FOUND);
        }
        finally {
            return null;
        }
    }

    @RequestMapping(params = LexemsCommands.COMMAND, method = RequestMethod.GET)
    public Object readAll(@RequestParam(value = LexemsCommands.COMMAND) String command) {

        try {
            switch (command) {
                case LexemsCommands.READ_ALL: {
                    logger.info("Вызов команды чтения всех объектов");
                    Orders orders = ioProxy.readAll(this.nameFile);
                    logger.info("Команда чтения всех объектов успешно выполнена");

                    //logger.debug("debug"); // all
                    //logger.info("info"); // except debug
                    //logger.warn("warn"); // except debug and info
                    //logger.error("error"); // except debug, info and warn
                    //logger.fatal("fatal"); // only fatal
                    return orders;
                }
                default: {
                    logger.error("Неизвестная команда! Код ошибки: " + ErrorCodes.COMMAND_NOT_FOUND);
                    return null;
                }
            }
        }
        catch(IOException ioexception){
            logger.error("Ошибка чтения файла " + this.nameFile);
            logger.error(ioexception.getMessage());
            return null;
        }
        catch (ClassNotFoundException classNotFound){
            logger.error("Ошибка синтаксического анализа файла" + this.nameFile + ". Класс не найден!");
            logger.error(classNotFound.getMessage());
            return null;
        }
    }
}