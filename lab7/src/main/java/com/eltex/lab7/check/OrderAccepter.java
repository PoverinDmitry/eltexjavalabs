package com.eltex.lab7.check;

import com.eltex.lab7.building_items.ACrudAction;
import com.eltex.lab7.building_items.BuildingItems;
import com.eltex.lab7.shopping_items.Credentials;
import com.eltex.lab7.shopping_items.Order;
import com.eltex.lab7.shopping_items.Orders;
import com.eltex.lab7.shopping_items.ShoppingCart;

import java.util.Iterator;
import java.util.Random;
import java.util.UUID;

public class OrderAccepter extends Thread {
    public OrderAccepter(Orders orders, Credentials credentials, BuildingItems buildingItems) {
        this.orders = orders;
        this.credentials = credentials;
        this.buildingItems = buildingItems;
    }

    private Orders<Order> orders = new Orders<>();
    private Credentials credentials;
    private BuildingItems buildingItems;

    private long waitingTime = 2 * 1000;

    private int outputMessage(String msg)
    {
        System.out.println(msg);
        return 0;
    }

    public Order makeOrder()
    {
        Random random = new Random();
        int countBuildingItems = buildingItems.getCount();
        ShoppingCart<ACrudAction> shoppingCart = new ShoppingCart<>();

            for (Iterator<ACrudAction> it = buildingItems.getIterator(); it.hasNext(); ) {
                ACrudAction item = it.next();
                if (random.nextBoolean())
                    shoppingCart.add(item);
            }
        return new Order(credentials, shoppingCart);
    }

    public int makeOrders()
    {
        Random random = new Random();
        int countBuildingItems = buildingItems.getCount();
        ShoppingCart<ACrudAction> shoppingCart = new ShoppingCart<>();

        synchronized (orders) {
            for (Iterator<ACrudAction> it = buildingItems.getIterator(); it.hasNext(); ) {
                ACrudAction item = it.next();
                if (random.nextBoolean())
                    shoppingCart.add(item);
            }
            UUID orderId = orders.makePurchase(credentials, shoppingCart);
            outputMessage("Заказ " + orderId.toString() + " принят!");
        }
        return 0;
    }

    @Override
    public void run()
    {
        while(true) {
            try {
                sleep(this.waitingTime);
                this.makeOrders();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}