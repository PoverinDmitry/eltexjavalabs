package com.eltex.lab7.check;

import com.eltex.lab7.shopping_items.Orders;

abstract public class ACheck extends Thread {

    public ACheck(Orders orders)
    {
        this.orders = orders;
        this.waitCheck = 15 * 1000;
    }
    public int setWaitCheck(long wc)
    {
        this.waitCheck = wc;
        return 0;
    }
    protected long waitCheck;
    protected volatile Orders orders;
    protected int outputInfo(String msg)
    {
        System.out.println(msg);
        return 0;
    }

    public abstract int check();

}
