package com.eltex.lab7;

import com.eltex.lab7.building_items.ACrudAction;
import com.eltex.lab7.building_items.BuildingItems;
import com.eltex.lab7.building_items.BuildingsMaterialsFactory;
import com.eltex.lab7.building_items.ItemFactory;
import com.eltex.lab7.shopping_items.Credentials;
import com.eltex.lab7.shopping_items.Order;
import com.eltex.lab7.shopping_items.Orders;
import com.eltex.lab7.shopping_items.ShoppingCart;

import java.util.Iterator;
import java.util.Random;
import java.util.UUID;

public class Helper {

    private static int countItems = 1;
    private static int countOrders = 10;

    public static BuildingItems createItems(){
        BuildingItems buildingItems = new BuildingItems();
        ItemFactory itemFactory = new BuildingsMaterialsFactory();

        for(int i = 0; i < countItems; i++)
        {
            ACrudAction item = (ACrudAction)itemFactory.createObject();
            item.create();
            buildingItems.add(item);
        }
        return buildingItems;
    }

    public static Credentials generateCred()
    {
        Credentials credentials = new Credentials(
                new StringBuilder("Иванов"),
                new StringBuilder("Иван"),
                new StringBuilder("Иванович"),
                new StringBuilder("ivanov@ivan.ru")
        );
        return credentials;
    }

    public static Orders makeOrders()
    {
        Orders<Order> orders = new Orders<>();
        BuildingItems buildingItems = createItems();
        Credentials credentials = generateCred();

        Random random = new Random();
        int countBuildingItems = buildingItems.getCount();
        ShoppingCart<ACrudAction> shoppingCart = new ShoppingCart<>();

        synchronized (orders) {
            for(int i = 0; i < countOrders; i++) {
                for (Iterator<ACrudAction> it = buildingItems.getIterator(); it.hasNext(); ) {
                    ACrudAction item = it.next();
                    if (random.nextBoolean())
                        shoppingCart.add(item);
                }
                UUID orderId = orders.makePurchase(credentials, shoppingCart);
            }
        }
        return orders;
    }
}