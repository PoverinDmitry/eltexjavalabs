package com.eltex.lab7.building_items;

public enum ItemTypeId {
    INSTRUMENTS,
    BUILDINGMAT,
    PAINTS;
}