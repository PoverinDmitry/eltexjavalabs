package com.eltex.lab7.building_items;

import com.eltex.lab7.building_items.ICrudAction;
import com.eltex.lab7.building_items.Intruments;
import com.eltex.lab7.building_items.ItemFactory;

public class IntrumentsFactory implements ItemFactory {
    @Override
    public ICrudAction createObject() {
        return new Intruments();
    }
}