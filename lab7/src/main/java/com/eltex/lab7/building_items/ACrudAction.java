package com.eltex.lab7.building_items;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.UUID;
import java.util.Random;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS,
        include = JsonTypeInfo.As.PROPERTY,
        property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BuildingMaterials.class),
        @JsonSubTypes.Type(value = Paints.class),
        @JsonSubTypes.Type(value = Intruments.class)
})
abstract public class ACrudAction implements ICrudAction {
    public ACrudAction()
    {
        this.ID = UUID.randomUUID();
    }

    private static int countObjects = 0;

    protected UUID ID;                  // Идентификатор
    protected StringBuilder name;       // Наименование товара
    protected StringBuilder vendorCode; // Артикул
    protected float cost;               // Цена
    protected StringBuilder companyMan; // Производитель

    protected static String divOutput = ":\t";
    protected static int minSizeRandomString = 5;
    protected static int maxSizeRandomString = 10;

    public static int getCountObjects() {
        return countObjects;
    }

    protected static StringBuilder getRandomString(int minSize, int maxSize)
    {
        Random random = new Random();

        int length;
        if(minSize != maxSize)
            length = random.nextInt(maxSize - minSize) + minSize;
        else
            length = minSize;

        StringBuilder result = new StringBuilder();
        char minChar = 'a';
        char maxChar = 'z';
        for(int i = 0; i < length; i++) {
            char ch = (char) ((char) random.nextInt(maxChar - minChar) + minChar);
            result.append(ch);
        }
        return result;
    }

    public UUID getID() {
        return ID;
    }

    @Override
    public int create()
    {
        countObjects++;
        this.name = this.getRandomString(this.minSizeRandomString, this.maxSizeRandomString);
        this.companyMan = this.getRandomString(this.minSizeRandomString, this.maxSizeRandomString);
        this.vendorCode = this.getRandomString(this.minSizeRandomString, this.maxSizeRandomString);
        this.cost = new Random().nextFloat();
        return 0;
    }

    @Override
    public int delete()
    {
        //this.ID = null;
        this.name = null;
        this.vendorCode = null;
        this.cost = 0.0f;
        this.companyMan = null;

        this.countObjects--;
        return 0;
    }

    @Override
    public int update() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String buf;

        System.out.println("Введите наименование товара");
        buf = in.readLine();
        this.name = new StringBuilder(buf);

        System.out.println("Введите артикул товара");
        buf = in.readLine();
        this.vendorCode = new StringBuilder(buf);

        System.out.println("Введите цену товара");
        buf = in.readLine();
        this.cost = Float.parseFloat(buf);

        System.out.println("Введите производителя товара");
        buf = in.readLine();
        this.companyMan = new StringBuilder(buf);
        in.close();
        return 0;
    }

    public static String outputValues(Object key, Object value, String div)
    {
        String result = key.toString() + div + value.toString();
        System.out.println(result);
        return result;
    }

    @Override
    public int read()
    {
        String div = this.divOutput;
        System.out.println("===========================================");
        outputValues("ID", this.ID, div);
        outputValues("Наименование", this.name, div);
        outputValues("Артикул", this.vendorCode, div);
        outputValues("Цена", this.cost, div);
        outputValues("Производитель", this.companyMan, div);

        return 0;
    }

}
