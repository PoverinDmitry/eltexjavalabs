package com.eltex.lab7.building_items;

public class BuildingsMaterialsFactory implements ItemFactory {
    @Override
    public ICrudAction createObject() {
        return new BuildingMaterials();
    }
}
