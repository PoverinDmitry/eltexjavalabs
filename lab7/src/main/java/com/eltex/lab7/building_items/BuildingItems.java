package com.eltex.lab7.building_items;
import java.io.Serializable;
import java.util.*;

public class BuildingItems implements Serializable {

    public BuildingItems()
    {
        this.CrudActionList = new LinkedList<>();
        this.CrudActionIdSet = new TreeSet<>();
    }

    public int add(ACrudAction item)
    {
        if(!this.CrudActionIdSet.contains(item.getID()))
        {
            this.CrudActionList.add(item);
            this.CrudActionIdSet.add(item.getID());
        }
        return 0;
    }

    public boolean itemIsAvailable(ACrudAction item)
    {
        return this.CrudActionIdSet.contains(item);
    }

    public int read()
    {
        for (ACrudAction item:this.CrudActionList
             ) {
            item.read();
        }
        return 0;
    }

    public int getCount()
    {
        return this.CrudActionList.size();
    }
    public ACrudAction getItem(int index)
    {
        return this.CrudActionList.get(index);
    }
    public Iterator<ACrudAction> getIterator()
    {
        return this.CrudActionList.iterator();
    }

    private List<ACrudAction> CrudActionList;
    private Set<UUID> CrudActionIdSet;
}