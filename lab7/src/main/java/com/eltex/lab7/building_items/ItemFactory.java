package com.eltex.lab7.building_items;

public interface ItemFactory {
    ICrudAction createObject();
}