package com.eltex.lab7.building_items;

public class PaintsFactory implements ItemFactory {
    @Override
    public ICrudAction createObject() {
        return new Paints();
    }
}
