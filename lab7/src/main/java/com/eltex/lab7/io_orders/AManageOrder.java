package com.eltex.lab7.io_orders;


import com.eltex.lab7.shopping_items.Orders;

abstract public class AManageOrder implements IOrder {
    public AManageOrder(Orders orders)
    {
        this.orders = orders;
    }

    protected Orders orders;

}
