package com.eltex.lab7.io_orders;

import com.eltex.lab7.building_items.ACrudAction;
import com.eltex.lab7.shopping_items.Order;
import com.eltex.lab7.shopping_items.OrderNotFoundException;
import com.eltex.lab7.shopping_items.Orders;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

public interface IOrder {
    Order readById(String fileName, UUID id) throws IOException, ClassNotFoundException, OrderNotFoundException;
    int saveById(String fileName, UUID id) throws IOException, ClassNotFoundException;
    Orders readAll(String fileName) throws IOException, ClassNotFoundException;
    int saveAll(String fileName) throws IOException;
    int addToCard(String fileName, ACrudAction item, UUID id) throws IOException, OrderNotFoundException;
    int delById(String fileName, UUID id) throws IOException, OrderNotFoundException;
}