package com.eltex.lab7.io_orders;


import com.eltex.lab7.building_items.ACrudAction;
import com.eltex.lab7.shopping_items.Order;
import com.eltex.lab7.shopping_items.OrderNotFoundException;
import com.eltex.lab7.shopping_items.Orders;
import com.eltex.lab7.shopping_items.ShoppingCart;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;


import java.io.*;
import java.util.Iterator;
import java.util.UUID;

public class ManagerOrderJSON extends AManageOrder {
    public ManagerOrderJSON(Orders orders) {
        super(orders);
        this.setDefaultObjectMapper();
    }

    private String tempFileName = "temp.json.tmp";
    private ObjectMapper objectMapper;

    public int setDefaultObjectMapper(){
        this.objectMapper = new ObjectMapper();
        this.objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        this.objectMapper.enableDefaultTyping();
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return 0;
    }

    public int setObjectMapper(ObjectMapper obj){
        this.objectMapper = obj;
        return 0;
    }

    @Override
    public Order readById(String fileName, UUID id) throws IOException {

        String result = "";
        FileInputStream fileInputStream = new FileInputStream(fileName);
        JsonParser jsonParser = objectMapper.getFactory().createParser(fileInputStream);
        try {
            for ( ; ; ) {
                Order tempOrder = objectMapper.readValue(jsonParser, Order.class);
                if(tempOrder.getId().equals(id)) {
                    return tempOrder;
                }
            }
        }
        catch(MismatchedInputException e)
        {
            fileInputStream.close();
            return null;
        }
    }

    @Override
    public int saveById(String fileName, UUID id) throws IOException {

        if(this.orders == null)
            throw new IOException("Коллекция не инициализирована");

        Order curOrder = this.orders.getOrderById(id);
        if(curOrder == null)
            return 1;

        UUID curId = curOrder.getId();

        String tempName = "temp.tmp";
        File tmp = new File(tempName);
        File in = new File(fileName);

        FileOutputStream fileOutputStream = new FileOutputStream(tmp, true);
        FileInputStream fileInputStream = new FileInputStream(in);
        JsonGenerator jsonGenerator = objectMapper.getFactory().createGenerator(fileOutputStream);
        JsonParser jsonParser = objectMapper.getFactory().createParser(fileInputStream);

        try {
            for ( ; ; ) {
                Order tempOrder = objectMapper.readValue(jsonParser, Order.class);
                if(curId.equals(tempOrder.getId()))
                {
                    objectMapper.writeValue(jsonGenerator, curOrder);
                    break;
                }
                else
                    objectMapper.writeValue(jsonGenerator, tempOrder);
            }
            for ( ; ; ) {
                Order tempOrder = objectMapper.readValue(jsonParser, Order.class);
                objectMapper.writeValue(jsonGenerator, tempOrder);
                break;
            }
        }
        catch(MismatchedInputException e)
        {
            fileInputStream.close();
            fileOutputStream.close();
            //in.delete();
            //tmp.renameTo(in);
        }
        return 0;
    }

    @Override
    public Orders readAll(String fileName) throws IOException {

        Orders ordersTemp = new Orders();
        FileInputStream fileInputStream = new FileInputStream(fileName);
        JsonParser jsonParser = objectMapper.getFactory().createParser(fileInputStream);
        try {
            for ( ; ; ) {
                Order tempOrder = objectMapper.readValue(jsonParser, Order.class);
                ordersTemp.add(tempOrder);
            }
        }
        catch(MismatchedInputException e)
        {
            fileInputStream.close();
            this.orders.copyFrom(ordersTemp);
        }
        finally {
            return ordersTemp;
        }
    }

    @Override
    public int saveAll(String fileName) throws IOException {
        if(this.orders == null)
            throw new IOException("Коллекция не инициализирована");
        FileOutputStream fileOutputStream = new FileOutputStream(fileName, true);
        JsonGenerator jsonGenerator = objectMapper.getFactory().createGenerator(fileOutputStream);
        for(Iterator<Order> it = this.orders.getIteratorList();it.hasNext();)
        {
            Order obj = it.next();
            objectMapper.writeValue(jsonGenerator, obj);
        }
        fileOutputStream.close();
        return 0;
    }

    @Override
    public int addToCard(String fileName, ACrudAction item, UUID id) throws IOException, OrderNotFoundException {

        String tempName = this.tempFileName;
        File tmp = new File(tempName);
        File in = new File(fileName);

        FileOutputStream fileOutputStream = new FileOutputStream(tmp, true);
        FileInputStream fileInputStream = new FileInputStream(in);
        JsonGenerator jsonGenerator = objectMapper.getFactory().createGenerator(fileOutputStream);
        JsonParser jsonParser = objectMapper.getFactory().createParser(fileInputStream);
        boolean idIsFound = false;
        try {
            for ( ; ; ) {
                Order tempOrder = objectMapper.readValue(jsonParser, Order.class);
                if(id.equals(tempOrder.getId()))
                {
                    idIsFound = true;
                    ShoppingCart shoppingCart = tempOrder.getShoppingCart();
                    shoppingCart.add(item);
                    objectMapper.writeValue(jsonGenerator, tempOrder);
                    break;
                }
                else
                    objectMapper.writeValue(jsonGenerator, tempOrder);
            }
            for ( ; ; ) {
                Order tempOrder = objectMapper.readValue(jsonParser, Order.class);
                objectMapper.writeValue(jsonGenerator, tempOrder);
                break;
            }
        }
        catch(MismatchedInputException e)
        {
            fileInputStream.close();
            fileOutputStream.close();

        }
        finally {
            in.delete();
            File file = new File(fileName);
            boolean flag = tmp.renameTo(file);
            if (!flag)
                throw new IOException("Невозможно переименовать файл");
            if (!idIsFound)
                throw new OrderNotFoundException();
        }
        return 0;
    }

    @Override
    public int delById(String fileName, UUID id) throws IOException, OrderNotFoundException {
        String tempName = this.tempFileName;
        File tmp = new File(tempName);
        File in = new File(fileName);

        boolean id_is_found = false;
        FileOutputStream fileOutputStream = new FileOutputStream(tmp, true);
        FileInputStream fileInputStream = new FileInputStream(in);
        JsonGenerator jsonGenerator = objectMapper.getFactory().createGenerator(fileOutputStream);
        JsonParser jsonParser = objectMapper.getFactory().createParser(fileInputStream);

        try {
            for ( ; ; ) {
                Order tempOrder = objectMapper.readValue(jsonParser, Order.class);
                if(id.equals(tempOrder.getId())){
                    id_is_found = true;
                    break;
                }
                else
                    objectMapper.writeValue(jsonGenerator, tempOrder);
            }
            for ( ; ; ) {
                Order tempOrder = objectMapper.readValue(jsonParser, Order.class);
                objectMapper.writeValue(jsonGenerator, tempOrder);
                break;
            }
        }
        catch(MismatchedInputException e)
        {
            fileInputStream.close();
            fileOutputStream.close();
        }
        finally {
            in.delete();
            File file = new File(fileName);
            boolean flag = tmp.renameTo(file);
            if(!flag)
                throw new IOException("Невозможно переименовать файл");
            if(!id_is_found)
                throw new OrderNotFoundException();
        }
        return 0;
    }
}
