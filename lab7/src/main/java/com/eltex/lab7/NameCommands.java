package com.eltex.lab7;

public enum NameCommands{
    READ_BY_ID("readById"),     // Лексема команды чтения заказа по идентификатору
    READ_ALL("readAll"),        // Лексема команды чтения всех заказов
    ADD_TO_CARD("addToCard"),   // Лексема команды добавления товара в корзину
    DEL_BY_ID("delById"),       // Лексема команды удаления заказа или товара по идетификатору

    COMMAND("command"),         // Лексема команды
    ORDER_ID("order_id"),       // Лексема заказа
    CARD_ID("cardId");          // Лексема товара

    private String text;

    NameCommands(String par) {
        this.text=par;
    }

    @Override
    public String toString()
    {
        return this.text;
    }

    public String method()
    {
        return "fg";
    }
}