package shopping_items;

import building_items.ACrudAction;

import java.util.Date;
import java.util.UUID;

public class Order {

    public Order(Credentials credentials, ShoppingCart shoppingCart)
    {
        this.isProcessed = false; // Принимает значение истина, если заказ обработан
        this.creationTime = new Date();

        this.shoppingCart = shoppingCart;
        this.credentials = credentials;
        this.id = UUID.randomUUID();
        this.waitingTime = (Date) this.creationTime.clone();
        this.waitingTime.setSeconds(59);
    }

    private UUID id;
    private Credentials credentials;
    private ShoppingCart shoppingCart;
    private boolean isProcessed;
    private Date creationTime;
    private Date waitingTime;

    public int updateSatus()
    {
        if(waitingTime.before(new Date()))
            isProcessed = true;
        return 0;
    }

    public boolean isOrderProcessed() {
        return this.isProcessed;
    }

    public int read()
    {
        String div = ":\t";
        System.out.println("===========Заказ=========");
        ACrudAction.outputValues("ID", this.id, div);
        ACrudAction.outputValues("Дата создания", this.creationTime, div);
        ACrudAction.outputValues("Дата исполнения", this.waitingTime, div);
        this.credentials.read();
        System.out.println("====Товары====");
        this.shoppingCart.read();
        return 0;
    }
}
