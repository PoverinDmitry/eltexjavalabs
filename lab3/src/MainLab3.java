import building_items.*;
import shopping_items.*;

import javax.print.attribute.standard.OrientationRequested;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainLab3 {

    private static final int SEL = 1; // SEL ~ SELECT Режим добаления товара в корзину
    private static final int PUR = 2; // PUR ~ PURCHASE Режим оформления покупки
    private static final int SHW = 3; // SHW ~ SHOW Показать все покупки
    private static final int SHW_ACT_OR = 4; // SHW_ACT_OR ~ SHOW ACTIVE ORDERS Показать все активные заказы
    private static final int EXT = 0; // EXT ~ EXIT Завершение работы программы

    private static int selectItem(BuildingItems items, ShoppingCart<ACrudAction> shoppingCart, BufferedReader in) throws IOException {
        int command;
        int countItems = items.getCount();


        boolean flag = true;
        do {

            System.out.println("Список товаров");
            items.read();
            System.out.println("=================================");
            System.out.println("Введите номер товара, который вы хотите добавить в корзину:\tот 1 до " + String.valueOf(countItems));
            System.out.println("Выход:\t0");
            command = Integer.parseInt(in.readLine());
            if(command == EXT)
                return 0;
            else if(command < 1 && command > countItems)
                System.out.println("Неверно введена команда");
            else
                shoppingCart.add(items.getItem(command - 1));
        } while(flag);
        return 0;
    }

    public static void main(String[] arg) throws IOException {

        BuildingItems buildingItems = new BuildingItems();
        int countItems = 5;
        ItemFactory itemFactory = new BuildingsMaterialsFactory();

        for(int i = 0; i < countItems; i++)
        {
            ACrudAction item = (ACrudAction)itemFactory.createObject();
            item.create();
            buildingItems.add(item);
        }

        System.out.println("Введите ИМЯ, ФАМИЛИЮ, ОТЧЕТВО, ваш email");

        String fname, lname, pat, email;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        fname = in.readLine();
        lname = in.readLine();
        pat = in.readLine();
        email= in.readLine();
        Credentials credentials = new Credentials(
                new StringBuilder(lname),
                new StringBuilder(fname),
                new StringBuilder(pat),
                new StringBuilder(email)
        );
        int command;

        boolean flag = true;

        ShoppingCart<ACrudAction> shoppingCart = new ShoppingCart<>();
        Orders<Order> orders = new Orders<>();
        do {
            System.out.println("=================================");
            System.out.println("Выбрать товар:\t1");
            System.out.println("Оформить покупку:\t2");
            System.out.println("Показать содержимое корзины:\t3");
            System.out.println("Показать все активные заказы:\t4");
            System.out.println("Выход:\t0");
            command = Integer.parseInt(in.readLine());

            switch(command)
            {
                case SEL:
                    selectItem(buildingItems, shoppingCart, in);
                    break;
                case PUR:
                    orders.makePurchase(credentials, shoppingCart);
                    shoppingCart = new ShoppingCart<>();
                    break;
                case SHW:
                    shoppingCart.read();
                    break;
                case SHW_ACT_OR:
                    orders.updateStatus();
                    orders.read();
                    break;
                case EXT:
                    flag = false;
                    break;
                default:
                    System.out.println("Неверная команда!");
            }
        } while(flag);
        in.close();
    }
}
