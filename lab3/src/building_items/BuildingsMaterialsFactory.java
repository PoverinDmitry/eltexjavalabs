package building_items;

public class BuildingsMaterialsFactory implements ItemFactory {
    @Override
    public ICrudAction createObject() {
        return new BuildingMaterials();
    }
}
