package shopping_items;

import java.util.*;

public class Orders {

    private List<Order> ordersList = new LinkedList<>();
    private Map<Date, Order> orderHashMap = new LinkedHashMap<>();

    public int makePurchase(Credentials credentials, ShoppingCart shoppingCart)
    {
        this.add(new Order(credentials, shoppingCart));
        return 0;
    }

    public int add(Order order)
    {
        this.ordersList.add(order);
        this.orderHashMap.put(new Date(), order);
        return 0;
    }
    public int delete(Order order)
    {
        this.ordersList.remove(order);
        this.orderHashMap.remove(order);
        return 0;
    }

    public int updateStatus()
    {
        for (Order item: ordersList)
        {
            item.updateSatus();
            if(item.isOrderProcessed())
                this.delete(item);
        }

        return 0;
    }

    public boolean isEmpty()
    {
        return this.ordersList.isEmpty();
    }

    public int read()
    {
        System.out.println("====Заказы====");
        if(this.ordersList.isEmpty()) {
            System.out.println("Пусто");
            return 0;
        }
        for (Order item: this.ordersList) {
            item.read();
        }
        return 0;
    }
}
