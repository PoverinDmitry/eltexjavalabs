package building_items;

public enum ItemTypeId {
    INSTRUMENTS,
    BUILDINGMAT,
    PAINTS;
}