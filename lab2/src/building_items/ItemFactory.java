package building_items;

public interface ItemFactory {
    ICrudAction createObject();
}