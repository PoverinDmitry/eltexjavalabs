package building_items;

import java.io.IOException;

public interface ICrudAction {
    int create();
    int read();
    int update() throws IOException;
    int delete();
}