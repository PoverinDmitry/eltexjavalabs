package io_orders;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import shopping_items.Order;
import shopping_items.Orders;

import java.io.*;
import java.util.Iterator;
import java.util.UUID;

public class ManagerOrderJSON extends AManageOrder {
    public ManagerOrderJSON(Orders orders) {
        super(orders);

    }
    @Override
    public int readById(String fileName, UUID id) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.enableDefaultTyping();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        FileInputStream fileInputStream = new FileInputStream(fileName);
        JsonParser jsonParser = objectMapper.getFactory().createParser(fileInputStream);
        try {
            for ( ; ; ) {
                Order tempOrder = objectMapper.readValue(jsonParser, Order.class);
                if(tempOrder.getId().equals(id)) {
                    if (this.orders.setOrderById(id, tempOrder) != 0)
                        this.orders.add(tempOrder);
                    break;
                }
            }
        }
        catch(MismatchedInputException e)
        {
            fileInputStream.close();
        }

        return 0;
    }

    @Override
    public int saveById(String fileName, UUID id) throws IOException {

        Order curOrder = this.orders.getOrderById(id);
        if(curOrder == null)
            return 1;

        UUID curId = curOrder.getId();

        String tempName = "temp.tmp";
        File tmp = new File(tempName);
        File in = new File(fileName);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.enableDefaultTyping();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        FileOutputStream fileOutputStream = new FileOutputStream(tmp, true);
        FileInputStream fileInputStream = new FileInputStream(in);
        JsonGenerator jsonGenerator = objectMapper.getFactory().createGenerator(fileOutputStream);
        JsonParser jsonParser = objectMapper.getFactory().createParser(fileInputStream);

        try {
            for ( ; ; ) {
                Order tempOrder = objectMapper.readValue(jsonParser, Order.class);
                if(curId.equals(tempOrder.getId()))
                {
                    objectMapper.writeValue(jsonGenerator, curOrder);
                    break;
                }
                else
                    objectMapper.writeValue(jsonGenerator, tempOrder);
            }
            for ( ; ; ) {
                Order tempOrder = objectMapper.readValue(jsonParser, Order.class);
                objectMapper.writeValue(jsonGenerator, tempOrder);
                break;
            }
        }
        catch(MismatchedInputException e)
        {
            fileInputStream.close();
            fileOutputStream.close();
            //in.delete();
            //tmp.renameTo(in);
        }
        return 0;
    }

    @Override
    public int readAll(String fileName) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.enableDefaultTyping();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Orders ordersTemp = new Orders();
        FileInputStream fileInputStream = new FileInputStream(fileName);
        JsonParser jsonParser = objectMapper.getFactory().createParser(fileInputStream);
        try {
            for ( ; ; ) {
                Order tempOrder = objectMapper.readValue(jsonParser, Order.class);
                ordersTemp.add(tempOrder);
            }
        }
        catch(MismatchedInputException e)
        {
            fileInputStream.close();
            this.orders.copyFrom(ordersTemp);
            ordersTemp = null;
        }
        return 0;
    }

    @Override
    public int saveAll(String fileName) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.enableDefaultTyping();

        FileOutputStream fileOutputStream = new FileOutputStream(fileName, true);
        JsonGenerator jsonGenerator = objectMapper.getFactory().createGenerator(fileOutputStream);
        for(Iterator<Order> it = this.orders.getIteratorList();it.hasNext();)
        {
            Order obj = it.next();
            objectMapper.writeValue(jsonGenerator, obj);
        }
        //objectMapper.writeValue(fileOutputStream, this.orders);
        fileOutputStream.close();
        return 0;
    }
}
