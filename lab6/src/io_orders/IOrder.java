package io_orders;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

public interface IOrder {
    int readById(String fileName, UUID id) throws IOException, ClassNotFoundException;
    int saveById(String fileName, UUID id) throws IOException, ClassNotFoundException;
    int readAll(String fileName) throws IOException, ClassNotFoundException;
    int saveAll(String fileName) throws IOException;
}
