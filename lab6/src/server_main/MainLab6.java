package server_main;

import building_items.*;
import check.ACheck;
import check.OrderAccepter;
import check.ProcessedCheck;
import check.WaitingCheck;
import client_server.OrderAccepterServer;
import client_server.OrderClient;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io_orders.IOProxy;
import io_orders.IOrder;
import io_orders.ManagerOrderFile;
import shopping_items.*;
import test.Circle;
import test.Shape;
import test.Triangle;

import javax.sound.midi.Instrument;
import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MainLab6 {

    private static final int SEL = 1; // SEL ~ SELECT Режим добаления товара в корзину
    private static final int PUR = 2; // PUR ~ PURCHASE Режим оформления покупки
    private static final int SHW = 3; // SHW ~ SHOW Показать все покупки
    private static final int SHW_ACT_OR = 4; // SHW_ACT_OR ~ SHOW ACTIVE ORDERS Показать все активные заказы
    private static final int EXT = 0; // EXT ~ EXIT Завершение работы программы

    private static int selectItem(BuildingItems items, ShoppingCart<ACrudAction> shoppingCart, BufferedReader in) throws IOException {
        int command;
        int countItems = items.getCount();

        boolean flag = true;
        do {

            System.out.println("Список товаров");
            items.read();
            System.out.println("=================================");
            System.out.println("Введите номер товара, который вы хотите добавить в корзину:\tот 1 до " + String.valueOf(countItems));
            System.out.println("Выход:\t0");
            command = Integer.parseInt(in.readLine());
            if(command == EXT)
                return 0;
            else if(command < 1 || command > countItems)
                System.out.println("Неверно введена команда");
            else
                shoppingCart.add(items.getItem(command - 1));
        } while(flag);
        return 0;
    }

    private static BuildingItems createItems()
    {
        BuildingItems buildingItems = new BuildingItems();
        int countItems = 10;
        ItemFactory itemFactory = new BuildingsMaterialsFactory();

        for(int i = 0; i < countItems; i++)
        {
            ACrudAction item = (ACrudAction)itemFactory.createObject();
            item.create();
            buildingItems.add(item);
        }
        return buildingItems;
    }

    private static Credentials inputCredentials(BufferedReader in) throws IOException {
        System.out.println("Введите ИМЯ, ФАМИЛИЮ, ОТЧЕТВО, ваш email");

        String fname, lname, pat, email;

        fname = in.readLine();
        lname = in.readLine();
        pat = in.readLine();
        email= in.readLine();
        Credentials credentials = new Credentials(
                new StringBuilder(lname),
                new StringBuilder(fname),
                new StringBuilder(pat),
                new StringBuilder(email)
        );
        return credentials;
    }

    public static void helperFunctionLabWork5() throws IOException, ClassNotFoundException {
        BuildingItems buildingItems = createItems();
        Credentials credentials = new Credentials(
                new StringBuilder("Иванов"),
                new StringBuilder("Иван"),
                new StringBuilder("Иванович"),
                new StringBuilder("ivanov@ivan.ru")
        );
        int command;
        boolean flag = true;

        ShoppingCart<ACrudAction> shoppingCart = new ShoppingCart<>();
        Orders<Order> orders = new Orders<>();
        Orders<Order> ordersCopy = new Orders<>();
        IOrder iOrder = new IOProxy(orders, IOProxy.FormatId.JSON);
        IOrder iOrderCopy = new IOProxy(ordersCopy, IOProxy.FormatId.JSON);

        OrderAccepter orderAccepter = new OrderAccepter(orders, credentials, buildingItems);
        // Принимаем три заказа
        orderAccepter.makeOrders();
        orderAccepter.makeOrders();
        orderAccepter.makeOrders();

        // Принимаем заказ отдельно, не добавляя его в коллецию
        Order ord = orderAccepter.makeOrder();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.enableDefaultTyping();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        String nameOrderFile = "Order.json";
        String nameOrdersFile = "Orders.json";

        FileOutputStream fileOutputStream = new FileOutputStream(nameOrderFile);
        objectMapper.writeValue(fileOutputStream, ord);
        fileOutputStream.close();

        iOrder.saveAll(nameOrdersFile);
        iOrderCopy.readAll(nameOrdersFile);

        iOrderCopy.readById(nameOrderFile, ord.getId());
        iOrderCopy.saveById(nameOrdersFile, ord.getId());

    }

    public static void main(String[] arg) throws IOException, ClassNotFoundException, InterruptedException {

        //helperFunctionLabWork5();
        OrderAccepterServer orderAccepterServer = new OrderAccepterServer(2);
        //OrderClient orderClient = new OrderClient();

        //orderClient.start();
        //orderClient.run();
        Thread.sleep(100);
        orderAccepterServer.serverWork();
    }
}
