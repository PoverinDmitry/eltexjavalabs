package server_main;

import client_server.OrderClient;

import java.net.UnknownHostException;

public class MainLab6Client {
    public static void main(String[] args) throws UnknownHostException {
        OrderClient orderClient = new OrderClient();
        orderClient.printMessage("Привет!");
        orderClient.clientWork();
    }
}
