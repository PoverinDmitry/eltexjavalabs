package building_items;

import java.io.IOException;
import java.io.Serializable;

public interface ICrudAction extends Serializable {
    int create();
    int read();
    int update() throws IOException;
    int delete();
}