package building_items;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Intruments extends ACrudAction {

    public Intruments()
    {
        super();
    }
    protected StringBuilder typeInstrument;

    @Override
    public int delete()
    {
        super.delete();
        this.typeInstrument = null;
        return 0;
    }

    @Override
    public int update() throws IOException {
        super.update();

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String buf;

        System.out.println("Введите тип интрумента (Ручной, электронный, измерительный и т.д.)");
        buf = in.readLine();
        this.typeInstrument = new StringBuilder(buf);
        in.close();

        return 0;
    }

    @Override
    public int create()
    {
        super.create();
        this.typeInstrument = this.getRandomString(this.minSizeRandomString, this.maxSizeRandomString);
        return 0;
    }

    @Override
    public int read()
    {
        super.read();
        String div = this.divOutput;
        outputValues("Тип интрумента", this.typeInstrument, div);
        return 0;
    }
}
