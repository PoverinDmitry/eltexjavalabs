package shopping_items;

import building_items.ACrudAction;


import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class Order implements Serializable {

    public Order()
    {

    }

    public Order(Credentials credentials, ShoppingCart shoppingCart)
    {
        this.isProcessed = ISWAITING; // Принимает значение истина, если заказ обработан
        this.creationTime = new Date();

        this.shoppingCart = shoppingCart;
        this.credentials = credentials;
        this.id = UUID.randomUUID();
        this.waitingTime = (Date) this.creationTime.clone();
        this.waitingTime.setSeconds(59);
    }

    public static final boolean ISPROCESSED = true; // Значение флага статуса заказа = обработано
    public static final boolean ISWAITING = false; // Значение флага статуса заказа = в обработке

    private UUID id;

    private Credentials credentials;

    private ShoppingCart shoppingCart;

    private boolean isProcessed;

    private Date creationTime;

    private Date waitingTime;

    public UUID getId()
    {
        return this.id;
    }

    public int setStatus(boolean status)
    {
        this.isProcessed = status;
        return 0;
    }

    public int updateSatus()
    {
        if(waitingTime.before(new Date()))
            isProcessed = true;
        return 0;
    }

    public boolean isOrderProcessed() {
        return this.isProcessed;
    }

    public int copyFrom(Order order)
    {
        this.isProcessed = order.isProcessed;
        this.creationTime = order.creationTime;
        this.credentials = order.credentials;
        this.waitingTime = order.waitingTime;
        this.shoppingCart = order.shoppingCart;

        return 0;
    }

    public int read()
    {
        String div = ":\t";
        System.out.println("===========Заказ=========");
        ACrudAction.outputValues("ID", this.id, div);
        ACrudAction.outputValues("Дата создания", this.creationTime, div);
        ACrudAction.outputValues("Дата исполнения", this.waitingTime, div);
        this.credentials.read();
        System.out.println("====Товары====");
        this.shoppingCart.read();
        return 0;
    }
}
