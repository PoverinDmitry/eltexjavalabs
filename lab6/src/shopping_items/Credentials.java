package shopping_items;

import building_items.ACrudAction;

import java.io.Serializable;
import java.util.UUID;

public class Credentials implements Serializable {
    public Credentials()
    {

    }
    public Credentials(StringBuilder ln,
                       StringBuilder fn,
                       StringBuilder pat,
                       StringBuilder email)
    {
        this.id = UUID.randomUUID();
        this.lastName = ln;
        this.firstName = fn;
        this.patronymic = pat;
        this.email = email;
    }
    protected UUID id;
    protected StringBuilder lastName;
    protected StringBuilder firstName;
    protected StringBuilder patronymic;
    protected StringBuilder email;

    public String geyInitials()
    {
        String result;
        result = lastName.toString() + " " + firstName.substring(0,1) + "." + patronymic.substring(0,1);
        return result;
    }

    public int read()
    {
        String div = ":\t";
        System.out.println("====Клиент====");
        ACrudAction.outputValues("ID", this.id, div);
        ACrudAction.outputValues("ИМЯ", this.firstName, div);
        ACrudAction.outputValues("ФАМИЛИЯ", this.lastName, div);
        ACrudAction.outputValues("ОТЧЕСТВО", this.patronymic, div);
        ACrudAction.outputValues("EMAIL", this.email, div);
        return 0;
    }
}