package check;

import building_items.ACrudAction;
import building_items.BuildingItems;
import client_server.ClientsIP;
import shopping_items.Credentials;
import shopping_items.Order;
import shopping_items.Orders;
import shopping_items.ShoppingCart;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Iterator;
import java.util.Random;
import java.util.UUID;

public class OrderAccepter extends Thread{
    public OrderAccepter(Orders orders, Credentials credentials, BuildingItems buildingItems) {
        this.orders = orders;
        this.credentials = credentials;
        this.buildingItems = buildingItems;
    }
    public OrderAccepter(Socket serverSocket)
    {
        this.serverSocket = serverSocket;
        this.orders = new Orders<>();
    }
    public OrderAccepter(Socket serverSocket, Orders orders, ClientsIP clientsIP)
    {
        this.serverSocket = serverSocket;
        this.orders = orders;
        this.clientsIP = clientsIP;
    }

    private Socket serverSocket;
    private Orders<Order> orders;
    private Credentials credentials;
    private BuildingItems buildingItems;
    private ClientsIP clientsIP;

    private long waitingTime = 2 * 1000;

    private int outputMessage(String msg)
    {
        System.out.println(msg);
        return 0;
    }

    public Order makeOrder()
    {
        Random random = new Random();
        int countBuildingItems = buildingItems.getCount();
        ShoppingCart<ACrudAction> shoppingCart = new ShoppingCart<>();

            for (Iterator<ACrudAction> it = buildingItems.getIterator(); it.hasNext(); ) {
                ACrudAction item = it.next();
                if (random.nextBoolean())
                    shoppingCart.add(item);
            }
        return new Order(credentials, shoppingCart);
    }

    public int makeOrders()
    {
        Random random = new Random();
        int countBuildingItems = buildingItems.getCount();
        ShoppingCart<ACrudAction> shoppingCart = new ShoppingCart<>();

        synchronized (orders) {
            for (Iterator<ACrudAction> it = buildingItems.getIterator(); it.hasNext(); ) {
                ACrudAction item = it.next();
                if (random.nextBoolean())
                    shoppingCart.add(item);

                UUID orderId = orders.makePurchase(credentials, shoppingCart);
                outputMessage("Заказ " + orderId.toString() + " принят!");
            }
        }
        return 0;
    }

    public int getOrder() throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(this.serverSocket.getInputStream());
        Order order = (Order) objectInputStream.readObject();
        outputMessage("Был получен заказ!");
        order.read();
        synchronized (orders)
        {
            synchronized (clientsIP) {
                orders.add(order);
                clientsIP.add(serverSocket.getInetAddress(), order.getId());
            }
        }
        return 0;
    }

    @Override
    public void run()
    {
            try {
                this.getOrder();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

    }
}