package check;

import building_items.ACrudAction;
import client_server.ClientsIP;
import shopping_items.Order;
import shopping_items.Orders;

import java.io.IOException;
import java.util.Iterator;

public class ProcessedCheck extends ACheck {
    public ProcessedCheck(Orders orders, ClientsIP clientsIP) {
        super(orders);
        this.clientsIP = clientsIP;
    }

    private ClientsIP clientsIP;

    @Override
    public int check() throws IOException {
        synchronized (orders) {
            for (Iterator<Order> it = this.orders.getIteratorList(); it.hasNext(); ) {
                Order item = it.next();
                if (item.isOrderProcessed()) {
                    this.clientsIP.sendMsg(item.getId()); // Оповещение и удаление адреса из колекции адресов клиентов
                    it.remove();
                    this.outputInfo("Заказ: " + item.getId().toString() + " удалён из базы");
                }
            }
        }
        return 0;
    }

    @Override
    public void run()
    {
        while(true) {
            try {
                sleep(this.waitCheck);
                System.out.println("Запущен обработчик статусов заказов...");
                this.check();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
