package client_server;

import building_items.ACrudAction;
import building_items.BuildingItems;
import building_items.BuildingsMaterialsFactory;
import building_items.ItemFactory;
import shopping_items.Credentials;

public class Helper {
    public static BuildingItems createItems(){
        BuildingItems buildingItems = new BuildingItems();
        int countItems = 10;
        ItemFactory itemFactory = new BuildingsMaterialsFactory();

        for(int i = 0; i < countItems; i++)
        {
            ACrudAction item = (ACrudAction)itemFactory.createObject();
            item.create();
            buildingItems.add(item);
        }
        return buildingItems;
    }

    public static Credentials generateCred()
    {
        Credentials credentials = new Credentials(
                new StringBuilder("Иванов"),
                new StringBuilder("Иван"),
                new StringBuilder("Иванович"),
                new StringBuilder("ivanov@ivan.ru")
        );
        return credentials;
    }
}
