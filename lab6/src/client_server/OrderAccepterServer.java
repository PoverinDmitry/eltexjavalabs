package client_server;

import check.ACheck;
import check.OrderAccepter;
import check.ProcessedCheck;
import check.WaitingCheck;
import shopping_items.Order;
import shopping_items.Orders;
import java.io.IOException;
import java.math.BigInteger;
import java.net.*;
import java.util.Enumeration;

public class OrderAccepterServer extends Thread {
    public OrderAccepterServer(int numberOfNetInterface) throws UnknownHostException {
        orders = new Orders<>();
        this.numberOfNetInterface = numberOfNetInterface;
        this.clientsIP = new ClientsIP(this.serverUdpPort);
    }

    private Orders<Order> orders;
    private ClientsIP clientsIP;
    private Integer serverUdpPort = 3333;
    private Integer serverTcpPort = 8981;
    private InetAddress serverAddres;
    private InetAddress serverAdressIpv6;
    private InetAddress broadcastAddress;
    private static String broadcastHost = "192.168.101.255";
    private static long waitForWaitingCheck = 1000;
    private static long waitForProcessCheck = 1000;

    private int numberOfNetInterface; // Номер сетевого интерфейса

    public int printMessage(String msg)
    {
        System.out.println("Server:" + this.serverAddres.toString() + ": " + msg);
        return 0;
    }

    private int getAddress() throws SocketException, UnknownHostException {
        Enumeration<NetworkInterface> gg = NetworkInterface.getNetworkInterfaces();

        NetworkInterface networkInterface = NetworkInterface.getByIndex(this.numberOfNetInterface);
        Enumeration<InetAddress> rin = networkInterface.getInetAddresses();
        //Object bindings = networkInterface.getInterfaceAddresses();

        this.broadcastAddress = InetAddress.getByName(this.broadcastHost);
        this.serverAdressIpv6 = rin.nextElement();
        this.serverAddres = rin.nextElement();
        return 0;
    }

    private int sendAddress() throws IOException {
        DatagramSocket datagramSocket = new DatagramSocket();
        datagramSocket.setBroadcast(true);
        System.out.println(datagramSocket.getInetAddress());

        byte[] msgBuffer;
        BigInteger bigInteger = BigInteger.valueOf(this.serverTcpPort);
        msgBuffer = bigInteger.toByteArray();

        this.broadcastAddress = InetAddress.getByName(broadcastHost);
        DatagramPacket datagramPacket = new DatagramPacket(msgBuffer, msgBuffer.length, this.broadcastAddress, this.serverUdpPort);

        datagramSocket.send(datagramPacket);
        datagramSocket.close();
        return 0;
    }

    public void serverWork() throws IOException {

        ACheck proccesedCheck = new ProcessedCheck(orders, clientsIP);
        ACheck waitingCheck = new WaitingCheck(orders);

        proccesedCheck.setWaitCheck(OrderAccepterServer.waitForProcessCheck);
        waitingCheck.setWaitCheck(OrderAccepterServer.waitForWaitingCheck);

        proccesedCheck.start();
        waitingCheck.start();

        try(ServerSocket serverSocket = new ServerSocket(this.serverTcpPort))
        {
            this.getAddress();
            this.sendAddress();
            for(;;) {

                printMessage("Жду клиентов...");
                Socket socket = serverSocket.accept();
                printMessage("Клиент принят!");
                OrderAccepter orderAccepter = new OrderAccepter(socket, orders, clientsIP);
                orderAccepter.start();
            }
        }
        catch(UnknownHostException e)
        {
            printMessage("Неизвестный хост");
        }
    }

    @Override
    public void run(){
        try {
            serverWork();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Невозможно выполнить рассылку");
        }
    }
}