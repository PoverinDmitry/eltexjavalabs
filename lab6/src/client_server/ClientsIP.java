package client_server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

public class ClientsIP {
    public ClientsIP(int port)
    {
        this.adresses = new TreeMap<>();
        this.port = port;
    }
    private Map<UUID, InetAddress> adresses;
    private int port;

    public int sendMsg(UUID id) throws IOException {
        InetAddress ip = this.adresses.get(id);
        this.adresses.remove(id);

        String msg = (new Date()).toString() + ": заказ обработан!";
        byte[] bytes = msg.getBytes();
        DatagramSocket datagramSocket = new DatagramSocket();
        DatagramPacket datagramPacket = new DatagramPacket(bytes,bytes.length, ip, this.port);
        datagramSocket.send(datagramPacket);
        datagramSocket.close();
        return 0;
    }

    public int add(InetAddress ip, UUID idOrder)
    {
        this.adresses.put(idOrder, ip);
        return 0;
    }
}
