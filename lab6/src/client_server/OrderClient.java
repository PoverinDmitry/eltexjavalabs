package client_server;

import building_items.ACrudAction;
import building_items.BuildingItems;
import check.OrderAccepter;
import shopping_items.Credentials;
import shopping_items.Order;
import shopping_items.Orders;
import shopping_items.ShoppingCart;

import java.io.*;
import java.net.*;

public class OrderClient extends Thread {

    private int udpPort = 3333;
    private int serverTcpPort;
    private InetAddress serverIP;
    private Credentials credentials;
    private String initialsClient;
    private static String broadcastHost = "255.255.255.255";
    private InetAddress broadcastAddress;

    public OrderClient() throws UnknownHostException {
        this.broadcastAddress = InetAddress.getByName(broadcastHost);
        this.generateName();
    }

    public int generateName()
    {
        this.credentials = new Credentials(
                ACrudAction.getRandomString(3, 10),
                ACrudAction.getRandomString(3, 10),
                ACrudAction.getRandomString(3, 10),
                ACrudAction.getRandomString(3, 10)
        );
        this.initialsClient = credentials.geyInitials();
        return 0;
    }

    private int getIntFromBytes(byte[] bytes) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        DataInputStream dataInputStream = new DataInputStream(byteArrayInputStream);
        Integer result = dataInputStream.readInt();
        int size = bytes.length;
        for(int i = size - 1; i>=0;i--)
        {
            if(bytes[i] != 0)
                break;
            else
                result = result >> 8;
        }

        return result;
    }

    public int printMessage(String msg)
    {
        System.out.println(this.initialsClient + ": " + msg);
        return 0;
    }

    private void udpGetMesTest()
    {
        try {
            DatagramSocket ds = new DatagramSocket(this.udpPort);
            boolean flag = true;
            while (flag) {

                int size = Integer.SIZE / 8;
                DatagramPacket pack = new DatagramPacket(new byte[size],  size);

                ds.receive(pack);

                String msg = new String(pack.getData());
                byte[] buf = pack.getData();

                this.serverTcpPort = this.getIntFromBytes(buf);
                this.serverIP = pack.getAddress();

                printMessage("Был получен номер порта: " + this.serverTcpPort);
                printMessage("IP: " + this.serverIP.toString());

                ds.close();
                flag = false;
            }
        }
        catch (BindException e){
            System.out.println(e);
        }
        catch(Exception e)   {
            System.out.println(e);
        }
    }

    private int recieveOrderStatus() throws IOException {
        printMessage("Жду оповещения о статусе заказа...");
        DatagramSocket datagramSocket = new DatagramSocket(this.udpPort);
        int buff_size = 1024;
        byte[] buff = new byte[buff_size];
        DatagramPacket datagramPacket = new DatagramPacket(buff, buff_size);
        datagramSocket.receive(datagramPacket);

        System.out.println(this.serverIP);
        printMessage("Было получено уведомление: " + new String(datagramPacket.getData()));

        return 0;
    }

    public int sendOrder(Order order)
    {
        printMessage(this.serverIP.toString());
        printMessage(Integer.toString(this.serverTcpPort));
        try(Socket socket = new Socket(this.serverIP, this.serverTcpPort))
        {
            OutputStream outputStream = socket.getOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(order);
            socket.close();
            printMessage("Заказ " + order.getId().toString() + " отправлен!");
            this.recieveOrderStatus();
        }
        catch (UnknownHostException e) {
            e.printStackTrace();
            this.printMessage("Не могу подключиться к серверу!");
            this.printMessage("IP: " + this.serverIP.toString());
            this.printMessage("Port: " + Integer.toString(this.serverTcpPort));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int clientWork() throws UnknownHostException {
        printMessage("Жду номер порта...");
        udpGetMesTest();
        BuildingItems buildingItems = Helper.createItems();
        Credentials credentials = Helper.generateCred();
        ShoppingCart<ACrudAction> shoppingCart = new ShoppingCart<>();
        Orders<Order> orders = new Orders<>();
        OrderAccepter orderAccepter = new OrderAccepter(orders, credentials, buildingItems);

        try {
            for(;;) {
                this.sendOrder(orderAccepter.makeOrder());
                recieveOrderStatus();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void run() {
        try {
            this.clientWork();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }
}